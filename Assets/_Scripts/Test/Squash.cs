﻿using System;
using UnityEngine;

public class Squash : MonoBehaviour
{
    public CollisionDetector cDetector;
    public Transform target;


    public float verticalScale = 1;

    [Range(0, 1f)]
    public float squashLerp;

    public float squashPower;

    public float squashThreshold = 10;

    public float squashRelease = 1.75f;

    public float squashDamping = 0.75f;

    private float currentSquashLerp = 0;



    private void Update()
    {
        float currentSquashPower = Mathf.Lerp(verticalScale, verticalScale / squashPower, squashLerp);

        float currentPosition = 0.5f + target.localScale.y * 0.5f;

        target.localScale = new Vector3(1, currentSquashPower, 1);

        target.localPosition = new Vector3(0, currentPosition, 0);

        currentSquashLerp = Mathf.Clamp01(currentSquashLerp - squashRelease * Time.deltaTime);

        squashLerp = Mathf.Lerp(squashLerp, currentSquashLerp, squashDamping * Time.deltaTime);
    }

    private void OnValidate()
    {
        if (target == null)
            return;

        if (squashPower < 1)
            squashPower = 1;
    }

    private void OnCollision(Vector3 obj)
    {
        currentSquashLerp = Mathf.Clamp(-obj.y / squashThreshold, 0, 1);
    }

    private void OnEnable()
    {
        cDetector.onCollision += OnCollision;
    }

    private void OnDisable()
    {
        cDetector.onCollision -= OnCollision;
    }
}