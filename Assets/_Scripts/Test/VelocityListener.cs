﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityListener : MonoBehaviour
{
    public Rigidbody rBody;

    public int savedVelocities = 3;

    private static Vector3[] lastVelocities;
    


    private void Awake()
    {
        lastVelocities = new Vector3[savedVelocities];
    }

    private void Update()
    {
        for (int i = 0; i < savedVelocities - 1; i++)
        {
            lastVelocities[i] = lastVelocities[i + 1];
        }

        lastVelocities[savedVelocities - 1] = rBody.velocity;
    }

    public static Vector3 GetAverageVelocity()
    {
        Vector3 average = Vector3.zero;

        for (int i = 0; i < lastVelocities.Length - 1; i++)
        {
            average += lastVelocities[i];
        }

        average /= lastVelocities.Length - 1;

        return average;
    }
}