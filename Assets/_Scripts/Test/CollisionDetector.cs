﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    public event Action<Vector3> onCollision;

    public Rigidbody rBody;



    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"Collided! Velocity is: {rBody.velocity}, average is: {VelocityListener.GetAverageVelocity()}");
        onCollision?.Invoke(rBody.velocity);
    }
}