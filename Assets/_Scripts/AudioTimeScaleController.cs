﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class AudioTimeScaleController : MonoBehaviour
{
    [Range(0, 1)]
    public float minimumTimeScale;
    public PlayableDirector audioTimeline;
    public AudioSource[] sources;

    private void OnTimeChanged(float currentTime)
    {
        currentTime = Mathf.Clamp(currentTime, minimumTimeScale, 1);

        // do something with the audio here
        if(audioTimeline.playableGraph.IsValid() && audioTimeline.playableGraph.IsPlaying())
            audioTimeline.playableGraph.GetRootPlayable(0).SetSpeed(currentTime);

        foreach (AudioSource source in sources)
        {
            source.pitch = currentTime;
        }
    }

    private void OnEnable()
    {
        TimeControlBehaviour.onTimeChanged += OnTimeChanged;
    }

    private void OnDisable()
    {
        TimeControlBehaviour.onTimeChanged -= OnTimeChanged;
    }
}