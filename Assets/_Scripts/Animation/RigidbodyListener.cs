﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyListener : MonoBehaviour
{
    public Animator Anima;

    public float AccelerationX
    {
        get { return deltaVelocity.x; }
    }
    public float AccelerationY
    {
        get { return deltaVelocity.y; } 
    }

    private Rigidbody rBody;
    private Vector3 lastVelocity;
    private Vector3 deltaVelocity;
    private Vector3 lastDeltaVelocity;
    private Vector3 deltaAcceleration;


    private void Awake()
    {
        rBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        UpdateVelocity();
    }

    private void UpdateVelocity()
    {
        Vector3 currentVelocity = rBody.velocity;

        deltaVelocity = currentVelocity - lastVelocity;

        deltaAcceleration = deltaVelocity - lastDeltaVelocity;

        lastVelocity = currentVelocity;
        lastDeltaVelocity = deltaVelocity;
    }

    public float debugLength;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        Gizmos.DrawRay(transform.position, deltaVelocity * debugLength);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position + Vector3.right, deltaAcceleration * debugLength);
    }
}