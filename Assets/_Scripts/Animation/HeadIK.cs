﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadIK : MonoBehaviour
{
    // referenses
    public Transform target;
    public Transform headBone;

    // parameter
    public bool isActive;
    [Range(0, 1)]
    public float weight;


    public void SetState(bool state)
    {
        isActive = state;
    }

    private void LateUpdate()
    {
        if(isActive)
        {
            RotateHead();
        }
    }

    private void RotateHead()
    {
        Vector3 direction = target.position - headBone.position;

        Quaternion lookRotation = Quaternion.LookRotation(direction, Vector3.up);

        headBone.rotation = Quaternion.Slerp(headBone.rotation, lookRotation, weight);
    }
}
