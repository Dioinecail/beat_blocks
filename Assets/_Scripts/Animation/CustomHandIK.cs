﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomHandIK : MonoBehaviour
{
    // referenses
    public Transform target;

    public Transform shoulderCloneAnchor, shoulderClone;
    public Transform elbowCloneAnchor, elbowClone;

    public Transform shoulder, elbow;

    // parameter
    public bool isActive;
    [Range(0, 1)]
    public float weight;



    public void Enable(bool state)
    {
        isActive = state;
    }

    private void LateUpdate()
    {
        if(isActive)
            ApplyIK();
    }

    private void ApplyIK()
    {
        // rotate the clone bone to match the forward vector with target direction
        Vector3 shoulderDirection = target.position - shoulderCloneAnchor.position;
        Quaternion shoulderTargetRotation = Quaternion.LookRotation(shoulderDirection, Vector3.up);
        shoulderCloneAnchor.rotation = shoulderTargetRotation;
        // clone the bone clone rotation to match mixamo's bone angles
        shoulder.rotation = Quaternion.Slerp(shoulder.rotation, shoulderClone.rotation, weight);


        Vector3 elbowDirection = target.position - elbowCloneAnchor.position;
        Quaternion elbowTargetRotation = Quaternion.LookRotation(elbowDirection, Vector3.up);
        elbowCloneAnchor.rotation = elbowTargetRotation;

        elbow.rotation = Quaternion.Slerp(elbow.rotation, elbowClone.rotation, weight);
    }
}