﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventSender : MonoBehaviour
{
    [Space]
    [Header("SFX")]
    public AudioClip[] Lose;
    public AudioClip[] Step;
    public AudioClip[] Jump;

    new public AudioSource audio;

    public UnityEvent onStep, onJump;

    public void OnStep()
    {
        //int randomNumber = Random.Range(0, Step.Length);

        //audio.PlayOneShot(Step[randomNumber]);
        onStep?.Invoke();
    }
    public void LoseEvent()
    {
        //int randomNumber = Random.Range(0, Lose.Length);

        //audio.PlayOneShot(Lose[randomNumber]);
    }
    public void OnJump()
    {
        //int randomNumber = Random.Range(0, Jump.Length);

        //audio.PlayOneShot(Jump[randomNumber]);
        onJump?.Invoke();
    }
}
