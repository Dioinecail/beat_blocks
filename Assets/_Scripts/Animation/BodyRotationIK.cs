﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyRotationIK : MonoBehaviour
{
    // referenses
    public Transform target;
    public Transform bodyBone;
    public Transform refTransform;

    // parameters
    public bool isActive;
    [Range(0, 1)]
    public float weight;
    public float maxAngle;
    public float angleThreshold;
    public Vector3 angleAxis;
    public Space rotationSpace;


    // cache
    private float currentAngle;


    public void SetState(bool state)
    {
        isActive = state;
    }

    private void LateUpdate()
    {
        if(isActive)
        {
            RotateBone();
        }
    }

    private void RotateBone()
    {
        currentAngle = CalculateAngleToTarget();

        if(currentAngle > angleThreshold)
        {
            currentAngle -= angleThreshold;
            float weightedAngle = Mathf.Clamp(currentAngle * weight, 0, maxAngle);

            bodyBone.Rotate(angleAxis, weightedAngle, rotationSpace);
        }
    }

    private float CalculateAngleToTarget()
    {
        Vector3 direction = target.position - refTransform.position;

        return Mathf.Abs(Vector3.SignedAngle(refTransform.forward, direction, Vector3.forward));
    }
}