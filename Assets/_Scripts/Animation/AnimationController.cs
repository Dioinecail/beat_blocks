﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public Animator anima;
    public Rigidbody rBody;

    public MoveBehaviour moveBehaviour;
    public CollisionBehaviour collisionBehaviour;
    public JumpBehaviour jumpBehaviour;

    private float velocityX;
    private float velocityY;
    private bool onGround;
    private bool onWall;



    private void Update()
    {
        UpdateVelocity();
        UpdateCollisionStates();
        UpdateAnimatorValues();
    }

    private void UpdateVelocity()
    {
        velocityX = Mathf.Abs(rBody.velocity.x);
        velocityY = rBody.velocity.y;
    }

    private void UpdateCollisionStates()
    {
        onGround = collisionBehaviour.onGround;
        onWall = collisionBehaviour.onWall;
    }

    private void UpdateAnimatorValues()
    {
        anima.SetFloat("VelocityX", velocityX);
        anima.SetFloat("VelocityY", velocityY);
        anima.SetBool("OnGround", onGround);
        //anima.SetBool("OnWall", onWall && !onGround);
    }

    private void OnJumpEvent()
    {
        anima.Play("Jump");
    }

    private void OnEnable()
    {
        JumpBehaviour.onJump += OnJumpEvent;
    }

    private void OnDisable()
    {
        JumpBehaviour.onJump -= OnJumpEvent;
    }
}