﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;

public class MovingCubeBehaviour : CubeBehaviour
{
    // parameters
    public Vector3 positionOffset;
    public float minimumSpeed;

    private Vector3 originalPosition;
    private bool isReturning;
    private float currentSpeed;



    protected override void Awake()
    {
        base.Awake();
        originalPosition = transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, (isReturning ? originalPosition : originalPosition + positionOffset), currentSpeed * Time.deltaTime);
    }

    public override void OnSignalRecieved(float speed)
    {
        currentSpeed = Mathf.Max(speed * signalMultiplier, minimumSpeed);

        if (Vector3.Distance(transform.position, (isReturning ? originalPosition : originalPosition + positionOffset)) < 0.1f)
            isReturning = !isReturning;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(transform.position, transform.position + positionOffset);
        Gizmos.DrawWireCube(transform.position + positionOffset, Vector3.one);
    }
}