﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;

public class JumpPadBehaviour : BaseSignalReciever
{
    // parameters
    public float jumpMultiplier = 2;
    public float threshold = 0.1f;

    // cache
    private Rigidbody playerBody;



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.SetParent(transform);
            playerBody = other.GetComponent<Rigidbody>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.SetParent(null);
            playerBody = null;
        }
    }

    public override void OnSignalRecieved(float signal)
    {
        if(playerBody != null && signal > threshold)
        {
            playerBody.velocity = transform.up * jumpMultiplier;
        }
    }
}