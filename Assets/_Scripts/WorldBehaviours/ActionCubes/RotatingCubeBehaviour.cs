﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCubeBehaviour : CubeBehaviour
{
    public float rotationSpeed = 30;
    public float minimumRotationSpeed;
    public Vector3 rotationAxis;
    public Transform rotatingTransform;



    public override void OnSignalRecieved(float signal)
    {
        rotatingTransform.Rotate(rotationAxis, Mathf.Max(rotationSpeed * signal * Time.deltaTime, minimumRotationSpeed * Time.deltaTime));
    }

    private void OnValidate()
    {
        minimumRotationSpeed = Mathf.Max(minimumRotationSpeed, 0);
    }
}