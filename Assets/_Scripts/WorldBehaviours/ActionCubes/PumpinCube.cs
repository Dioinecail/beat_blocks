﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PumpinCube : CubeBehaviour
{
    // parameters
    public Vector3 positionOffset;
    public float velocityMult;

    private Vector3 originalPosition;



    protected override void Awake()
    {
        base.Awake();
        originalPosition = transform.position;
    }

    public override void OnSignalRecieved(float value)
    {
        transform.position = Vector3.Lerp(originalPosition, originalPosition + positionOffset, value);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(transform.position, transform.position + positionOffset);
        Gizmos.DrawWireCube(transform.position + positionOffset, Vector3.one);
    }
}