﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;

public abstract class CubeBehaviour : BaseSignalReciever
{
    protected virtual void OnTriggerEnter(Collider other)
    {
        //if (other.tag == "Player")
        //    other.transform.SetParent(transform);
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        //if (other.tag == "Player")
        //    other.transform.SetParent(null);
    }
}

#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(CubeBehaviour), editorForChildClasses: true)]
public class CubeBehaviourEditor : UnityEditor.Editor
{
    private const string materialsPath = "Assets/_Materials/_Audio/";
    private const string materialName = "Material_AudioPulse_";

    CubeBehaviour cube;



    private void OnEnable()
    {
        cube = target as CubeBehaviour;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        if (GUILayout.Button("SetMaterial"))
        {
            List<int> selected = cube.GetID().GetSelected();

            string id = ((ReceiverID)selected[0]).ToString();

            string materialPath = materialsPath + materialName + id + ".mat";

            Material mat = UnityEditor.AssetDatabase.LoadAssetAtPath<Material>(materialPath);

            MeshRenderer targetRenderer = cube.GetComponentInChildren<MeshRenderer>();
            Material[] sharedMaterials = targetRenderer.sharedMaterials;
            sharedMaterials[1] = mat;
            targetRenderer.sharedMaterials = sharedMaterials;
        }
    }
}

#endif