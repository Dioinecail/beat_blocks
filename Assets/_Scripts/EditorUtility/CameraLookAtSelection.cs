﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.ProBuilder;
using UnityEngine.ProBuilder;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteAlways]
public class CameraLookAtSelection : MonoBehaviour
{
    public bool isLooking;
    public bool subscribe;
    public bool ubsubscribe;

#if UNITY_EDITOR
    private void Update()
    {
        if (!isLooking)
            return;
    }
#endif

    private void OnValidate()
    {
        if (subscribe)
            SubscribeToSelectionChangedEvent();

        if (ubsubscribe)
            UnsubscribeToSelectionChangedEvent();
    }

    private void SubscribeToSelectionChangedEvent()
    {
        subscribe = false;
        ProBuilderMesh.elementSelectionChanged += OnSelectionChanged;
    }

    private void UnsubscribeToSelectionChangedEvent()
    {
        ubsubscribe = false;
        ProBuilderMesh.elementSelectionChanged -= OnSelectionChanged;
    }

    private void OnSelectionChanged(ProBuilderMesh mesh)
    {
        Transform meshTransform = mesh.GetComponent<Transform>();

        if(mesh.selectedVertexCount > 0)
        {
            var vertices = mesh.GetVertices();
            var vertice = mesh.selectedVertices[0];

            Vector3 position = vertices[vertice].position;
            Vector3 positionWorld = meshTransform.TransformPoint(position);

            transform.position = new Vector3(positionWorld.x, positionWorld.y, transform.position.z);
        }
    }
}