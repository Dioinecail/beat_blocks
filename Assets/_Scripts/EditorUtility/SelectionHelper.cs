﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class SelectionHelper : MonoBehaviour
{
    public static bool isHelping = false;

#if UNITY_EDITOR

    [MenuItem("EditorUtility/Enable selection helpers")]
    public static void EnableHelping()
    {
        isHelping = true;
    }

    [MenuItem("EditorUtility/Disable selection helpers")]
    public static void DisableHelping()
    {
        isHelping = false;
    }

    private void Update()
    {
        if (Selection.activeGameObject == gameObject && isHelping)
            Selection.activeGameObject = transform.parent.gameObject;
    }
#endif
}