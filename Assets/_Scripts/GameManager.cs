﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Vector2 activeSpawnPoint;

    public GameObject player;
    public int defaultLevel;




    private void Awake()
    {
        LevelManager.Init();

        if (!LevelManager.AnythingLoaded())
        {
            LevelManager.LoadStartingLevel(defaultLevel);
        }
        else
        {
            player.transform.position = activeSpawnPoint;
        }
    }

    private void OnBeforeUnloadingLevels()
    {
        player.transform.SetParent(null);
    }

    private void OnEnable()
    {
        LevelManager.onBeforeUnloadingLevels += OnBeforeUnloadingLevels;
    }

    private void OnDisable()
    {
        LevelManager.onBeforeUnloadingLevels -= OnBeforeUnloadingLevels;
    }
}