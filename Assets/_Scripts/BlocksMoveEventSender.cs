﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksMoveEventSender : MonoBehaviour
{
    public BlockManager BM;
    public PulseEventSender pulser;
    public AudioSource audioSource;
    public Animation animation;

    public float BeatDelay;
    public float FirstDelay;
    public float RotationSpeed;

    private bool shouldMove;

    Coroutine rotateCoroutine;

    public void Awake()
    {
        audioSource.Play(0);
        animation.Play();
        //StartCoroutine(MoveBlockOnMusic(BeatDelay, FirstDelay));
    }

    public void MakeBlocksMove()
    {
        if (BM != null && pulser != null)
        {
            //pulser.SendPulse();

            //if(shouldMove)
                BM.SendMessage("MoveStuff");
            //shouldMove = !shouldMove;
        }
    }


    IEnumerator RotateLight(Transform lightTransform)
    {
        if (lightTransform != null)
        {
            Quaternion targetRotation = Quaternion.Euler(0, 0, 90) * lightTransform.rotation;
            for (int i = 0; i < 30; i++)
            {
                if (lightTransform != null)
                    lightTransform.rotation = Quaternion.Lerp(lightTransform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
                else
                    break;
                yield return new WaitForEndOfFrame();
            }
        }
    }

    IEnumerator MoveBlockOnMusic(float beatDelay, float firstDelay = 0)
    {
        yield return new WaitForSeconds(beatDelay + firstDelay);

        MakeBlocksMove();

        yield return MoveBlockOnMusic(beatDelay);
    }
}
