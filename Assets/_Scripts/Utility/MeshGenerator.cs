﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    public static Mesh CreateMesh(int sizeX, int sizeY)
    {
        Vector3[] vertices = new Vector3[(sizeX + 1) * (sizeY + 1)];
        int[] triangles = new int[sizeX * sizeY * 6];
        Vector2[] uv = new Vector2[vertices.Length];

        for (int i = 0, y = 0; y <= sizeY; y++)
        {
            for (int x = 0; x <= sizeX; x++, i++)
            {
                vertices[i] = new Vector3(.5f - (float)x / sizeX, 0.5f - (float)y/ sizeY);
                uv[i] = new Vector2((float)x / sizeX, (float)y / sizeY);
            }
        }

        for (int ti = 0, vi = 0, y = 0; y < sizeY; y++, vi++)
        {
            for (int x = 0; x < sizeX; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + sizeX + 1;
                triangles[ti + 5] = vi + sizeX + 2;
            }
        }


        Mesh newMesh = new Mesh();
        newMesh.SetVertices(vertices);
        newMesh.SetTriangles(triangles, 0);
        newMesh.uv = uv;

        return newMesh;
    }
}