﻿public interface IKillable
{
    float HP { get; set; }

    void RecieveDamage(float damage);
    void OnDie();
}