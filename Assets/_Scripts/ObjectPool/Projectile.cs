﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PoolableBase
{
    // referense;
    public new SphereCollider collider;
    public GameObject explosionParticlePrefab;

    // parameters
    public LayerMask collisionMask;
    private Vector3 direction;
    private float damage;

    // cache
    private Collider[] collisionBuffer = new Collider[1];


    private void Update()
    {
        CheckCollision();
        transform.position += direction * Time.deltaTime;
    }

    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }

    private void CheckCollision()
    {
        collisionBuffer[0] = null;

        Physics.OverlapSphereNonAlloc(transform.position, collider.radius, collisionBuffer, collisionMask);

        if(collisionBuffer[0] != null)
        {
            // do action
            IKillable hitObject = collisionBuffer[0].GetComponent<IKillable>();

            if (hitObject != null)
                hitObject.RecieveDamage(damage);

            //StunBehaviour stunBehaviour = collisionBuffer[0].GetComponent<StunBehaviour>();

            //stunBehaviour?.Stun();

            // emit particle
            ParticleEmitter.EmitParticle(explosionParticlePrefab, transform.position);

            // return to pool
            ReturnToPool();
        }
    }
}