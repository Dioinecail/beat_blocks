﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    // parameters
    public string playerTag;
    public bool oneTimeUse;
    public UnityEvent onTriggerEvent;

    // cache
    private bool used;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == playerTag && !used)
        {
            used = oneTimeUse;

            onTriggerEvent?.Invoke();
        }
    }
}