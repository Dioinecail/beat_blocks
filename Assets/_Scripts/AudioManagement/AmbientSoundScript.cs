﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using FMODUnity;
using System;
using FMOD;

public class AmbientSoundScript : MonoBehaviour
{
    // Attributes for the lights that we want to change
    public Light light;
    public float mult;
    public int dspIndex;

    // FMOD stuff
    public StudioEventEmitter emitter;
    EventInstance emitterInstance;

    private void Start()
    {
        emitter.Play();
        emitterInstance = emitter.EventInstance;
    }

    private void Update()
    {
        SetChannelGroupMeteringEnabled(emitterInstance, true);
        Bus busA = RuntimeManager.GetBus("bus:/A_Group");
        light.intensity = GetOutputLevel(busA) * mult;
    }

    float GetOutputLevel(EventInstance instance)
    {
        ChannelGroup group;
        DSP dsp;
        DSP_METERING_INFO output = new DSP_METERING_INFO();

        RESULT result = instance.getChannelGroup(out group);
        group.getDSP(dspIndex, out dsp);
        result = dsp.getMeteringInfo(IntPtr.Zero, out output);
        return output.rmslevel[0];
    }

    float GetOutputLevel(Bus bus)
    {
        ChannelGroup group;
        DSP dsp;
        DSP_METERING_INFO output = new DSP_METERING_INFO();

        Channel chan;

        RESULT result = bus.getChannelGroup(out group);
        group.getDSP(dspIndex, out dsp);
        result = dsp.getMeteringInfo(IntPtr.Zero, out output);
        return output.rmslevel[0];
    }

    void SetChannelGroupMeteringEnabled(EventInstance instance, bool isEnabled)
    {
        ChannelGroup group;
        DSP dsp;

        instance.getChannelGroup(out group);
        group.getDSP(0, out dsp);
        RESULT result = dsp.setMeteringEnabled(false, isEnabled);
    }
}