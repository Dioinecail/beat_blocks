﻿using System;
using UnityEngine;
using System.Runtime.InteropServices;
using FMODUnity;
using FMOD;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class FMOD_DSP : MonoBehaviour
{
    // referenses
    public LineRenderer lineRenderer;
    public StudioEventEmitter eventEmitter;

    // parameters
    [Range(0, 1)] public float WIDTH = 1f;
    [Range(0.01f, 0.5f)] public float HEIGHT = 0.1f;
    public float yOffset;
    public string[] paramNames;

    // cache
    private List<LineRenderer> debugRenderers= new List<LineRenderer>();


    private void Start()
    {
        CreateDebugRenderers();
        eventEmitter.Play();
    }

    private void Update()
    {
        for (int i = 0; i < paramNames.Length; i++)
        {
            float value = GetLevelFromParameter(eventEmitter, paramNames[i]);
            debugRenderers[i].SetPosition(0, new Vector3(i * WIDTH, yOffset + HEIGHT * value));
        }
    }

    public void CreateDebugRenderers()
    {
        for (int i = 0; i < paramNames.Length; i++)
        {
            LineRenderer r = Instantiate(lineRenderer);
            r.SetPosition(1, new Vector3(i * WIDTH, yOffset));
            debugRenderers.Add(r);
        }
    }

    public float GetLevelFromParameter(StudioEventEmitter emitter, string parameterName)
    {
        RESULT getParameterByNameResult = emitter.EventInstance.getParameterByName(parameterName, out float value, out float finalValue);
        return finalValue;
    }
}