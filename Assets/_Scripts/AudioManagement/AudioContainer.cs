﻿namespace AudioManagement
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class AudioContainer : MonoBehaviour
    {
        // referenses
        public List<AudioObject> audioObjects { get; set; }

        // cache
        private static string[] currentIds;



        private void Awake()
        {
            if (audioObjects == null || audioObjects.Count == 0)
                audioObjects = GetComponentsInChildren<AudioObject>().ToList();
        }

        public string[] GetIdsFromAudioObjects()
        {
            List<string> ids = new List<string>();

            if (audioObjects == null || audioObjects.Count == 0)
                audioObjects = GetComponentsInChildren<AudioObject>().ToList();

            foreach (var audioObject in audioObjects)
            {
                foreach (FrequencyData data in audioObject.frequencyList)
                {   
                    if(!ids.Contains(data.name))
                        ids.Add(data.name);
                }
            }

            currentIds = ids.ToArray();

            return ids.ToArray();
        }

        public static string [] GetIds()
        {
            return currentIds;
        }

        public List<AudioSource> GetSources()
        {
            List<AudioSource> sources = new List<AudioSource>();

            for (int i = 0; i < audioObjects.Count; i++)
            {
                sources.Add(audioObjects[i].source);
            }

            return sources;
        }
    }
}