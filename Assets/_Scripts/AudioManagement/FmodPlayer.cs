﻿using UnityEngine;
using FMODUnity;
using FMOD;
using System.Collections.Generic;
using System;

[Flags]
public enum ReceiverID
{
    KICK_0,
    KICK_1,
    HIHAT_0,
    HIHAT_1,
    SNARE_0,
    SNARE_1,
    MAIN,
    SUB,
    BASS
}

public static class EnumExtensions
{
    public static List<int> GetSelected<ReceiverID>(this ReceiverID target)
    {
        List<int> selectedIndexes = new List<int>();
        Array enumValues = Enum.GetValues(typeof(ReceiverID));

        for (int i = 0; i < enumValues.Length; i++)
        {
            int layer = 1 << i;
            if ((Convert.ToInt32(target) & layer) != 0)
            {
                selectedIndexes.Add(i);
            }
        }
        return selectedIndexes;
    }
}

public class EnumFlagsAttribute : PropertyAttribute
{
    public EnumFlagsAttribute() { }
}

#if UNITY_EDITOR
[UnityEditor.CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
public class EnumFlagsAttributeDrawer : UnityEditor.PropertyDrawer
{
    public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
    {
        property.intValue = UnityEditor.EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
    }
}
#endif

[Serializable]
public class OutputData
{
    public string name;
    [EnumFlags]
    public ReceiverID targetId;
    [SerializeField] private float peak;
    public float Peak { get { return peak; } set { peak = value; } }
    public float Signal { get; set; }
    public float RelativeSignal { get { return Signal / peak; } }

    public bool calibrate;

}

public class FmodPlayer : MonoBehaviour
{
    // events
    public static event Action<ReceiverID, float> outputEvent;

    // referenses
    public static FmodPlayer Instance;
    public StudioEventEmitter eventEmitter;

    // parameters
    public float baseVolume = 0.5f;
    public bool persistBetweenLevels;
    public bool playOnStart;
    public OutputData[] outputs;



    public float GetLevelFromParameter(StudioEventEmitter emitter, string parameterName)
    {
        if (!emitter.IsPlaying())
            return -1;

        RESULT getParameterByNameResult = emitter.EventInstance.getParameterByName(parameterName, out float value, out float finalValue);
        return finalValue;
    }

    public void SetVolume(float value)
    {
        eventEmitter.EventInstance.setVolume(value);
    }

    private void Start()
    {
        if (playOnStart)
        {
            eventEmitter.Play();
            eventEmitter.EventInstance.setVolume(baseVolume);
        }

        if (persistBetweenLevels)
        {
            if (Instance == null)
            {
                Instance = this;

                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < outputs.Length; i++)
        {
            outputs[i].Signal = GetLevelFromParameter(eventEmitter, outputs[i].name);
            if (outputs[i].calibrate)
                Calibrate(outputs[i]);
            outputEvent?.Invoke(outputs[i].targetId, outputs[i].RelativeSignal);
        }
    }

    private void Calibrate(OutputData data)
    {
        if (data.Peak < data.Signal)
            data.Peak = data.Signal;
    }
}
