﻿namespace AudioManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu]
    public class ReceiverPreset : ScriptableObject
    {
        [Range(0, 1)] public float signalFalloffMin = 0.001f;
        [Range(0, 1)] public float signalFalloffMax = 0.01f;
        [Range(0, 1)] public float signalFalloffLerp = 0.178f;
        public float damping = 25;
        public float dampingThreshold = 0.35f;



        private void OnValidate()
        {
            signalFalloffMin = Mathf.Min(signalFalloffMin, signalFalloffMax);
            signalFalloffMax = Mathf.Max(signalFalloffMin, signalFalloffMax);
        }
    }
}