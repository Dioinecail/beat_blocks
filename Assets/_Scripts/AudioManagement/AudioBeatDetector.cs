﻿namespace AudioManagement
{
    using System.Collections.Generic;
    using UnityEngine;

    public delegate void OnBeatDetected(FrequencyData data);

    public class AudioBeatDetector : MonoBehaviour
    {
        // events
        public static event OnBeatDetected onContinuousBeatEvent;
        public static event OnBeatDetected onSingleBeatEvent;

        //referenses
        public AudioContainer container;



        private void Update()
        {
            ProcessAudio();
        }

        private void ProcessAudio()
        {
            foreach (var aObject in container.audioObjects)
            {
                aObject.GetSamples();
                aObject.GetAllSignals();

                foreach (var data in aObject.frequencyList)
                {
                    onContinuousBeatEvent?.Invoke(data);
                }
            }
        }
    }
}