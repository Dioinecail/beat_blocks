﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class DebugLine
{
    public ReceiverID signalId;
    public LineRenderer line;
}

public class FMOD_DebugLevels : MonoBehaviour
{
    // referenses
    public FmodPlayer targetPlayer;
    public List<DebugLine> debugRenderers = new List<DebugLine>();

    // parameters
    public float lineWidth;
    public float lineHeight;

    // cache



    public void RecieveOutput(ReceiverID id, float value)
    {
        if(debugRenderers.Any(x => x.signalId == id))
        {
            LineRenderer r = debugRenderers.First(x => x.signalId == id).line;
            r.SetPosition(0, new Vector3(0, lineHeight * value));
        }
    }

    private void OnEnable()
    {
        FmodPlayer.outputEvent += RecieveOutput;
    }

    private void OnDisable()
    {
        FmodPlayer.outputEvent -= RecieveOutput;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(FMOD_DebugLevels))]
public class FMOD_DebugLevelsEditor : Editor
{
    private FMOD_DebugLevels debugLevels;

    private void OnEnable()
    {
        debugLevels = (FMOD_DebugLevels)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Create renderers"))
        {
            debugLevels.debugRenderers = new List<DebugLine>();
            FmodPlayer player = debugLevels.targetPlayer;
            OutputData[] outputs = player.outputs;

            for (int i = 0; i < outputs.Length; i++)
            {
                DebugLine line = new DebugLine();
                line.signalId = outputs[i].targetId;
                line.line = new GameObject("LineRenderer_" + line.signalId).AddComponent<LineRenderer>();
                debugLevels.debugRenderers.Add(line);
            }
        }
    }
}
#endif