﻿namespace AudioManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class ThresholdArrayReciever : BaseSignalReciever
    {
        [Range(0, 1)]
        public float threshold;

        public int startingIndex = 0;
        public UnityEvent[] onAction;

        private int index;
        private bool actionLocker;



        protected override void Awake()
        {
            base.Awake();

            index = startingIndex;
        }

        public override void OnSignalRecieved(float signal)
        {
            if (!actionLocker)
            {
                if (signal > threshold)
                {
                    actionLocker = true;
                    onAction[index]?.Invoke();
                    index++;

                    if (index > onAction.Length - 1)
                        index = 0;
                }
            }
            else
            {
                if (signal < threshold)
                {
                    actionLocker = false;
                }
            }
        }
    }
}