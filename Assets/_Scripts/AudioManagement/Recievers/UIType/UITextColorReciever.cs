﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;
using UnityEngine.UI;

public class UITextColorReciever : BaseSignalReciever
{
    public Text text;
    public Color originalColor, targetColor;



    private void Awake()
    {
        originalColor = text.color;
    }

    public override void OnSignalRecieved(float signal)
    {
        text.color = Color.Lerp(originalColor, targetColor, signal * signalMultiplier);
    }
}