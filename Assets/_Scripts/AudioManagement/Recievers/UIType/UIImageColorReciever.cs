﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;
using UnityEngine.UI;

public class UIImageColorReciever : BaseSignalReciever
{
    public Image icon;
    private Color originalColor, targetColor;

    private void Awake()
    {
        originalColor = icon.color;
    }

    public override void OnSignalRecieved(float signal)
    {
        icon.color = Color.Lerp(originalColor, targetColor, signal * signalMultiplier);
    }
}