﻿namespace AudioManagement
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SignalID : IEquatable<string>
    {
        public string id;
        public int selectedIndex;

        public bool Equals(string other)
        {
            return id.Equals(other);
        }

        public override string ToString()
        {
            return id;
        }
    }

    public static class SignalIDList
    {
        public static bool Contains(this List<SignalID> list, string id)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(id))
                    return true;
            }

            return false;
        }
    }
}