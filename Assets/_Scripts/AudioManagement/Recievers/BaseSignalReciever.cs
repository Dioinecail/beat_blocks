﻿namespace AudioManagement
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class BaseSignalReciever : MonoBehaviour
    {
        [SerializeField, EnumFlags]
        protected ReceiverID signalIds;

        public float signalMultiplier = 1;
        public ReceiverPreset receiverPreset;

        protected float[] lastSignal;
        protected float[] currentFalloff;
        protected float[] buffer;



        public virtual void OnAudioRecieved(ReceiverID id, float value)
        {
            List<int> selected = signalIds.GetSelected();

            for (int i = 0; i < selected.Count; i++)
            {
                if (signalIds == id)
                    buffer[i] = ProcessSignal(value, i);
            }

            float maxValue = 0;

            for (int i = 0; i < buffer.Length; i++)
            {
                maxValue = Mathf.Max(buffer[i], maxValue);
            }

            OnSignalRecieved(maxValue);
        }

        public virtual void OnSignalRecieved(float signal)
        {

        }

        public ReceiverID GetID()
        {
            return signalIds;
        }

        protected virtual void Awake()
        {
            CreateValuesBuffers();
        }

        protected void CreateValuesBuffers()
        {
            List<int> selected = signalIds.GetSelected();

            buffer = new float[selected.Count];
            lastSignal = new float[selected.Count];
            currentFalloff = new float[selected.Count];
        }

        protected float ProcessSignal(float relativeSignal, int index)
        {
            float deltaSignal = Mathf.Abs(lastSignal[index] - relativeSignal);

            if(lastSignal[index] > relativeSignal)
            {
                currentFalloff[index] =  Mathf.Clamp(currentFalloff[index] + receiverPreset.signalFalloffLerp, receiverPreset.signalFalloffMin, receiverPreset.signalFalloffMax);
                lastSignal[index] = Mathf.Lerp(lastSignal[index], relativeSignal, currentFalloff[index]);
            }
            else
            {
                float dampedSmoothing = 1;

                if (deltaSignal > receiverPreset.dampingThreshold)
                    dampedSmoothing /= receiverPreset.damping;

                lastSignal[index] = Mathf.Lerp(lastSignal[index], relativeSignal, dampedSmoothing);

                currentFalloff[index] = receiverPreset.signalFalloffMin;
            }

            return lastSignal[index];
        }

        protected void OnEnable()
        {
            FmodPlayer.outputEvent += OnAudioRecieved;
        }

        protected void OnDisable()
        {
            FmodPlayer.outputEvent -= OnAudioRecieved;
        }

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            if (receiverPreset == null)
            {
                string[] guids = UnityEditor.AssetDatabase.FindAssets("l:Receiver_default");

                string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]);

                ReceiverPreset defaultPreset = UnityEditor.AssetDatabase.LoadAssetAtPath<ReceiverPreset>(path);

                receiverPreset = defaultPreset;
            }
        }
#endif
    }
}