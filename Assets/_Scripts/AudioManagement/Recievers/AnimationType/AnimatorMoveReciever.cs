﻿namespace AudioManagement
{
    using UnityEngine;

    public class AnimatorMoveReciever : BaseSignalReciever
    {
        // referenses
        public Animator Anima;

        // parameters
        public float animatorMinimumSpeed = 0.2f;



        public override void OnSignalRecieved(float signal)
        {
            Anima.speed = Mathf.Max(signal * signalMultiplier, animatorMinimumSpeed);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
                other.transform.SetParent(transform);
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
                other.transform.SetParent(null);
        }
    }
}