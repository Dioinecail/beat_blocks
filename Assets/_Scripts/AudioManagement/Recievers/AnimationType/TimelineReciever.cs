﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;
using UnityEngine.Playables;

public class TimelineReciever : BaseSignalReciever
{
    public PlayableDirector targetPlayableDirector;



    public override void OnSignalRecieved(float signal)
    {
        targetPlayableDirector.time = targetPlayableDirector.duration * Mathf.Clamp01(signal * signalMultiplier);
    }
}