﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;

public class AnimatedRecieverBehaviour : BaseSignalReciever
{
    public Animation targetAnimation;



    private void Start()
    {
        targetAnimation[targetAnimation.clip.name].weight = 1;
        targetAnimation.Play();
        targetAnimation[targetAnimation.clip.name].speed = 0;
    }

    public override void OnSignalRecieved(float signal)
    {
        targetAnimation[targetAnimation.clip.name].normalizedTime = Mathf.Clamp01(signal * signalMultiplier);
    }
}