﻿namespace AudioManagement
{
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(SignalID))]
    public class SignalIDEditor : PropertyDrawer
    {
        string[] ids = new string[0];

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, GUIContent.none, property);

            SerializedProperty id = property.FindPropertyRelative("id");
            SerializedProperty selectedIndex = property.FindPropertyRelative("selectedIndex");

            FmodPlayer container = Object.FindObjectOfType<FmodPlayer>();

            if (container != null)
            {
                ids = GetIds(container);
            }
            else
                return;

            EditorGUI.BeginChangeCheck();

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            selectedIndex.intValue = EditorGUI.Popup(position, selectedIndex.intValue, ids);

            if (selectedIndex.intValue > -1)
            {
                if (selectedIndex.intValue > ids.Length)
                    selectedIndex.intValue = 0;

                selectedIndex.intValue = Mathf.Clamp(selectedIndex.intValue, 0, ids.Length - 1);

                id.stringValue = ids[selectedIndex.intValue];
            }

            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }
            EditorGUI.EndProperty();
        }

        public string[] GetIds(FmodPlayer player)
        {
            return player.outputs.Select(x => x.name).ToArray();
        }
    }
}