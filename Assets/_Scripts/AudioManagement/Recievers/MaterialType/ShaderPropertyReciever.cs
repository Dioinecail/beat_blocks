﻿namespace AudioManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public enum PropertyType
    {
        Float,
        Vector,
        Color
    }

    [System.Serializable]
    public class SignalReceiver
    {
        // referenses
        public Material targetMaterial;

        // parameters
        public PropertyType propertyType;
        public string propertyName;

        [Header("Vector Parameters")]
        public Vector3 vectorA;
        public Vector3 vectorB;

        [Header("Color Parameters")]
        public Color colorA;
        public Color colorB;

        [SerializeField, EnumFlags]
        private ReceiverID receiverID;

        public float signalMultiplier = 1;
        public ReceiverPreset receiverPreset;

        private float[] lastSignal;
        private float[] currentFalloff;
        private float[] buffer;



        public void CreateBuffers()
        {
            List<int> selected = receiverID.GetSelected();

            buffer = new float[selected.Count];
            lastSignal = new float[selected.Count];
            currentFalloff = new float[selected.Count];
        }

        public void ProcessEvent(ReceiverID id, float value)
        {
            List<int> selected = receiverID.GetSelected();

            for (int i = 0; i < selected.Count; i++)
            {
                if (receiverID == id)
                    buffer[i] = ProcessSignal(value, i);
            }

            float maxValue = 0;

            for (int i = 0; i < buffer.Length; i++)
            {
                maxValue = Mathf.Max(buffer[i], maxValue);
            }

            switch (propertyType)
            {
                case PropertyType.Float:
                    targetMaterial.SetFloat(propertyName, maxValue * signalMultiplier);
                    break;
                case PropertyType.Vector:
                    Vector3 position = Vector3.LerpUnclamped(vectorA, vectorB, maxValue * signalMultiplier);
                    targetMaterial.SetVector(propertyName, position);
                    break;
                case PropertyType.Color:
                    Color c = Color.LerpUnclamped(colorA, colorB, maxValue * signalMultiplier);
                    targetMaterial.SetColor(propertyName, c);
                    break;
                default:
                    break;
            }
        }

        protected float ProcessSignal(float relativeSignal, int index)
        {
            float deltaSignal = Mathf.Abs(lastSignal[index] - relativeSignal);

            if (lastSignal[index] > relativeSignal)
            {
                currentFalloff[index] = Mathf.Clamp(currentFalloff[index] + receiverPreset.signalFalloffLerp, receiverPreset.signalFalloffMin, receiverPreset.signalFalloffMax);
                lastSignal[index] = Mathf.Lerp(lastSignal[index], relativeSignal, currentFalloff[index]);
            }
            else
            {
                float dampedSmoothing = 1;

                if (deltaSignal > receiverPreset.dampingThreshold)
                    dampedSmoothing /= receiverPreset.damping;

                lastSignal[index] = Mathf.Lerp(lastSignal[index], relativeSignal, dampedSmoothing);

                currentFalloff[index] = receiverPreset.signalFalloffMin;
            }

            return lastSignal[index];
        }
    }

    public class ShaderPropertyReciever : MonoBehaviour
    {
        [SerializeField]
        private SignalReceiver[] shaderPropertyReceivers;



        private void OnAudioRecieved(ReceiverID id, float RelativeSignal)
        {
            for (int i = 0; i < shaderPropertyReceivers.Length; i++)
            {
                shaderPropertyReceivers[i].ProcessEvent(id, RelativeSignal);
            }
        }

        #region UNITY_EVENTS

        protected void Awake()
        {
            for (int i = 0; i < shaderPropertyReceivers.Length; i++)
            {
                shaderPropertyReceivers[i].CreateBuffers();
            }
        }

        private void OnEnable()
        {
            FmodPlayer.outputEvent += OnAudioRecieved;
        }

        private void OnDisable()
        {
            FmodPlayer.outputEvent -= OnAudioRecieved;
        }

        #endregion
    }
}