﻿namespace AudioManagement
{
    using UnityEngine;
    using UnityEngine.Events;

    public class ThresholdReciever : BaseSignalReciever
    {
        [Range(0, 1)]
        public float threshold;

        public UnityEvent onAction;

        private bool actionLocker;



        public override void OnSignalRecieved(float signal)
        {
            if(!actionLocker)
            {
                if(signal > threshold)
                {
                    actionLocker = true;
                    onAction?.Invoke();
                }
            }
            else
            {
                if(signal < threshold)
                {
                    actionLocker = false;
                }
            }
        }
    }
}