﻿namespace AudioManagement
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class BaseMultiSignalsReciever : MonoBehaviour
    {
        [SerializeField]
        protected List<SignalID> signalIds;

        public float signalMultiplier = 1;



        protected virtual void OnAudioRecieved(FrequencyData data)
        {
            if (signalIds.Any(x => x.Equals(data.name)))
            {
                OnSignalRecieved(data);
            }
        }

        protected virtual void OnSignalRecieved(FrequencyData data)
        {

        }

        protected void OnEnable()
        {
            AudioBeatDetector.onContinuousBeatEvent += OnAudioRecieved;
        }

        protected void OnDisable()
        {
            AudioBeatDetector.onContinuousBeatEvent -= OnAudioRecieved;
        }
    }
}