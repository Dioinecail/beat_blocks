﻿namespace AudioManagement
{
    using UnityEngine;

    public class VectoralReciever : BaseSignalReciever
    {
        public Vector3 offset;
        private Vector3 originalPosition;



        private void Awake()
        {
            originalPosition = transform.position;
        }

        public override void OnSignalRecieved(float signal)
        {
            transform.position = Vector3.Lerp(originalPosition, originalPosition + offset, signal * signalMultiplier);
        }
    }
}