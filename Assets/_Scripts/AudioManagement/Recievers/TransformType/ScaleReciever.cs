﻿namespace AudioManagement
{
    using UnityEngine;

    public class ScaleReciever : BaseSignalReciever
    {
        public float scale;
        private Vector3 originalScale;



        private void Awake()
        {
            originalScale = transform.localScale;
        }

        public override void OnSignalRecieved(float signal)
        {
            transform.localScale = Vector3.Lerp(originalScale, originalScale * scale, signal * signalMultiplier);
        }
    }
}