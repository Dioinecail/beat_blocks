﻿namespace AudioManagement
{
    using UnityEngine;

    public class HeroCapeColorVolume : RecieverVolume
    {
        public Material capeMaterial;
        public Color targetColor;

        protected override void OnTriggered(Transform player)
        {
            float relativeDistance = 1 - Vector3.Distance(transform.position, player.position) / maxDistance;

            capeMaterial.SetColor("_BaseColor", Color.Lerp(Color.black, targetColor, relativeDistance * currentSignal));
        }

        private void OnDrawGizmosSelected()
        {
            if (volume == null)
                volume = GetComponent<SphereCollider>();

            Gizmos.color = targetColor;
            Gizmos.DrawSphere(transform.position, volume.radius);
        }
    }
}