﻿namespace AudioManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(SphereCollider))]
    public class RecieverVolume : BaseSignalReciever
    {
        public string playerTag;
        protected float currentSignal;
        protected float maxDistance;

        protected SphereCollider volume;

        private void Awake()
        {
            volume = GetComponent<SphereCollider>();

            maxDistance = volume.radius;
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.tag != playerTag)
                return;

            OnTriggered(other.transform);
        }

        protected virtual void OnTriggered(Transform player)
        {

        }

        public override void OnSignalRecieved(float signal)
        {
            currentSignal = signal * signalMultiplier;
        }
    }
}