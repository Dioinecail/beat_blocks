﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioManagement;

public class FrequencyPairRandomizer : MonoBehaviour
{
    public AudioContainer container;

    // time in seconds between randomizations
    public float randomizeInterval;
    // selectable frequencies
    public float minFrequency, maxFrequency;
    // sectionSize
    public float minSection, maxSection;
    // frequency pairs amount
    public int pairsAmount;

    private AudioObject randomizableObject;

    private void Awake()
    {
        randomizableObject = CreateRandomizableObject();
        // dont use the drums thing, because it's quite simple
        //randomizableObject.source = container.GetSources()[Random.Range(1, container.GetSources().Count)];

        container.audioObjects.Add(randomizableObject);

        StartCoroutine(RandomizeTimer(randomizeInterval));
    }

    private AudioObject CreateRandomizableObject()
    {
        AudioObject newObject = new AudioObject();
        newObject.samplesCount = 1024;
        newObject.Samples = new float[newObject.samplesCount];

        for (int i = 0; i < pairsAmount; i++)
        {
            newObject.frequencyList.Add(CreateFrequencyPair());
        }
        // create stuff
        return newObject;
    }

    private FrequencyData CreateFrequencyPair()
    {
        FrequencyData pair = new FrequencyData();

        return pair;
    }

    private void RandomizeFrequencyPair(FrequencyData pair)
    {
        float frequency = Random.Range(minFrequency, maxFrequency);
        // randomize stuff
    }

    private IEnumerator RandomizeTimer(float interval)
    {
        yield return new WaitForSeconds(interval);


        yield return RandomizeTimer(interval);
    }
}