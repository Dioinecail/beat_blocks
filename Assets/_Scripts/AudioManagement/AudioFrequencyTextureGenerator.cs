﻿using System.Collections;
using System.Collections.Generic;
using AudioManagement;
using UnityEngine;

public class AudioFrequencyTextureGenerator : BaseSignalReciever
{
    public List<ReceiverID> freqList;
    public List<float> signals;

    public Material mat;

    private Texture2D frequencyTexture;



    private void Awake()
    {
        for (int i = 0; i < freqList.Count; i++)
        {
            signals.Add(0);
        }

        frequencyTexture = new Texture2D(freqList.Count, 1, TextureFormat.Alpha8, false);
        SetShaderProperties();
    }

    public override void OnAudioRecieved(ReceiverID id, float value)
    {
        int index = -1;

        for (int i = 0; i < freqList.Count; i++)
        {
            if(freqList[i] == id)
            {
                index = i;
                break;
            }
        }

        if(index > -1)
            OnRecievedNamedSignal(id, value);
    }

    private void OnRecievedNamedSignal(ReceiverID id, float value)
    {
        int index = freqList.FindIndex(x => x.Equals(id));
        signals[index] = value;

        GenerateTexture();
    }

    void GenerateTexture()
    {
        for (int x = 0; x < frequencyTexture.width; x++)
        {
            for (int y = 0; y < frequencyTexture.height; y++)
            {
                frequencyTexture.SetPixel(x, y, new Color(0, 0, 0, signals[x]));
            }
        }

        frequencyTexture.Apply();
    }

    void SetShaderProperties()
    {
        mat.SetTexture("_FrequencyTexture", frequencyTexture);
    }
}