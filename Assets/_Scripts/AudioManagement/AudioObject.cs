﻿namespace AudioManagement
{
    using System.Collections.Generic;
    using UnityEngine;

    public class AudioObject : MonoBehaviour
    {
        public AudioSource source;
        public int samplesCount = 1024;
        public List<FrequencyData> frequencyList;

        private float[] samples;
        public float[] Samples
        {
            get
            {
                if (samples == null || samples.Length != samplesCount)
                {
                    samples = new float[samplesCount];
                }

                return samples;
            }
            set
            {
                samples = value;
            }
        }



        private void Update()
        {
            foreach (var freq in frequencyList)
            {
                if (freq.calibrate)
                {
                    freq.Calibrate();
                }
            }
        }

        public void GetSamples()
        {
            source.GetSpectrumData(Samples, 0, FFTWindow.Hamming);
        }

        public void GetSignal(FrequencyData data)
        {
            data.currentCleanSignal = 0;
            //data.RelativeSignal = 0;

            int minIndex = Mathf.Clamp(Mathf.RoundToInt((Samples.Length - 1) * (data.frequency - data.frequencyRange)), 0, samplesCount);
            int maxIndex = Mathf.Clamp(Mathf.RoundToInt((Samples.Length - 1) * (data.frequency + data.frequencyRange)), 0, samplesCount);

            for (int i = minIndex; i <= maxIndex; i++)
                if(source.volume > 0)
                    if ((Samples[i] / source.volume) > data.threshold)
                        data.currentCleanSignal = Mathf.Max(Samples[i] / source.volume, data.currentCleanSignal);

            data.RelativeSignal = data.currentCleanSignal / data.signalPeak;
        }

        public void GetAllSignals()
        {
            for (int i = 0; i < frequencyList.Count; i++)
                GetSignal(frequencyList[i]);
        }

        public void PopulateData(string baseName, float step, float segmentStart, int segmentCount)
        {
            frequencyList = new List<FrequencyData>();

            for (int i = 0; i < segmentCount; i++)
            {
                FrequencyData pair = new FrequencyData();
                pair.name = baseName + i.ToString();
                pair.frequency = segmentStart + step * i;
                pair.signalPeak = 0.02377465f;
                frequencyList.Add(pair);
            }
        }

        private void OnValidate()
        {
            foreach (var freq in frequencyList)
            {
                if (freq.resetCalibration)
                {
                    freq.resetCalibration = false;
                    freq.ResetCalibration();
                }
            }
        }
    }


    [System.Serializable]
    public class FrequencyData
    {
        public string name;
        public float signalPeak;
        public float currentCleanSignal;

        [Range(0, 0.025f)] public float threshold;
        public bool calibrate;
        public bool resetCalibration;

        [Range(0, 1)] public float frequency;
        [Range(0, 1)] public float frequencyRange;

        /// <summary> Gets a signal relative to peak value (0-1) </summary>
        public float RelativeSignal { get; set; }



        public void ResetCalibration()
        {
            signalPeak = 0;
        }

        public void Calibrate()
        {
            if (signalPeak < currentCleanSignal)
                signalPeak = currentCleanSignal;
        }
    }
}