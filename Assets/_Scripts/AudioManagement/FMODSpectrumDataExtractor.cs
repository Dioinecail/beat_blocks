﻿using System;
using UnityEngine;
using System.Runtime.InteropServices;
using FMODUnity;

class FMODSpectrumDataExtractor : MonoBehaviour
{
    public string mainEventName;
    public string[] eventNames;
    [Range(0, 30)]
    public float WIDTH = 10.0f;
    [Range(0.01f, 0.5f)]
    public float HEIGHT = 0.1f;
    public int spectrumIndex;

    FMOD.Studio.EventInstance musicInstance;
    FMOD.Studio.EventInstance[] eventInstances;
    FMOD.DSP[] fft;

    public LineRenderer lineRendererPrefab;
    public LineRenderer[] lineRenderers;

    const int WindowSize = 1024;

    public string channelName;
    public int namelen;



    void Start()
    {
        lineRendererPrefab.positionCount = (WindowSize);
        lineRendererPrefab.startWidth = .1f;
        lineRendererPrefab.endWidth = .1f;

        musicInstance = RuntimeManager.CreateInstance(mainEventName);
        eventInstances = new FMOD.Studio.EventInstance[eventNames.Length];
        fft = new FMOD.DSP[eventNames.Length];

        for (int i = 0; i < fft.Length; i++)
        {
            RuntimeManager.CoreSystem.createDSPByType(FMOD.DSP_TYPE.FFT, out fft[i]);
            fft[i].setParameterInt((int)FMOD.DSP_FFT.WINDOWTYPE, (int)FMOD.DSP_FFT_WINDOW.HANNING);
            fft[i].setParameterInt((int)FMOD.DSP_FFT.WINDOWSIZE, WindowSize * 2);
        }

        lineRenderers = new LineRenderer[eventInstances.Length];
        FMOD.ChannelGroup mainChannelGroup;
        RuntimeManager.CoreSystem.getMasterChannelGroup(out mainChannelGroup);

        for (int i = 0; i < eventInstances.Length; i++)
        {
            eventInstances[i] = RuntimeManager.CreateInstance(eventNames[i]);
            FMOD.ChannelGroup channelGroup;
            eventInstances[i].getChannelGroup(out channelGroup);
            channelGroup.addDSP(FMOD.CHANNELCONTROL_DSP_INDEX.HEAD, fft[i]);

            lineRenderers[i] = Instantiate(lineRendererPrefab);
        }
        musicInstance.start();
        mainChannelGroup.getNumChannels(out namelen);
    }


    void Update()
    {

        for (int i = 0; i < fft.Length; i++)
        {
            IntPtr unmanagedData;
            uint length;
            fft[i].getParameterData((int)FMOD.DSP_FFT.SPECTRUMDATA, out unmanagedData, out length);
            FMOD.DSP_PARAMETER_FFT fftData = (FMOD.DSP_PARAMETER_FFT)Marshal.PtrToStructure(unmanagedData, typeof(FMOD.DSP_PARAMETER_FFT));
            var spectrum = fftData.spectrum;

            if (fftData.numchannels > 0)
            {
                var pos = Vector3.zero;
                pos.x = WIDTH * -0.5f;

                for (int j = 0; j < WindowSize; ++j)
                {
                    pos.x += (WIDTH / WindowSize);

                    float level = lin2dB(spectrum[spectrumIndex][j]);
                    pos.y = (80 + level) * HEIGHT;
                    pos.z = i;

                    lineRenderers[j].SetPosition(j, pos);
                }
            }
        }
    }

    float lin2dB(float linear)
    {
        return Mathf.Clamp(Mathf.Log10(linear) * 20.0f, -80.0f, 0.0f);
    }
}