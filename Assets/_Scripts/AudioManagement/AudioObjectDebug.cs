﻿namespace AudioManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AudioObjectDebug : MonoBehaviour
    {
        // referenses
        public Material debugMaterial;
        public AudioObject targetObject;

        // parameters
        public float debugMultiply;
        public int frequencyIndex;

        // cached
        private Texture2D texture;



        private void Awake()
        {
            texture = new Texture2D(targetObject.samplesCount, 2,
                UnityEngine.Experimental.Rendering.DefaultFormat.HDR,
                UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            debugMaterial.SetTexture("_DebugTexture", texture);
        }

        private void Update()
        {
            DebugAudio();
        }

        private void DebugAudio()
        {
            targetObject.GetSamples();
            targetObject.GetAllSignals();

            int minIndex = 0;
            int maxIndex = 0;

            if(frequencyIndex < targetObject.frequencyList.Count)
            {
                FrequencyData data = targetObject.frequencyList[frequencyIndex];

                minIndex = Mathf.Clamp(Mathf.RoundToInt((targetObject.Samples.Length - 1) * (data.frequency - data.frequencyRange)), 0, targetObject.samplesCount);
                maxIndex = Mathf.Clamp(Mathf.RoundToInt((targetObject.Samples.Length - 1) * (data.frequency + data.frequencyRange)), 0, targetObject.samplesCount);
            }

            for (int i = 0; i < targetObject.Samples.Length; i++)
            {
                for (int x = 0; x < texture.height; x++)
                {
                    if (i >= minIndex && i <= maxIndex)
                        texture.SetPixel(i, x, new Color(targetObject.Samples[i] * debugMultiply, 1, targetObject.frequencyList[frequencyIndex].RelativeSignal));
                    else 
                        texture.SetPixel(i, x, new Color(targetObject.Samples[i] * debugMultiply, 0, 0));
                }
            }

            texture.Apply();
            debugMaterial.SetTexture("_DebugTexture", texture);
        }

        private void OnDisable()
        {
            if (texture != null)
                Destroy(texture);
        }

        private void OnDestroy()
        {
            if (texture != null)
                Destroy(texture);
        }
    }
}