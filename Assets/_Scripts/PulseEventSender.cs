﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnPulse(float power);

public class PulseEventSender : MonoBehaviour
{
    public static event OnPulse onPulse;

    public float pulseSpeed = 0.1f;
    public float pulseDuration = 0.65f, pulsePower = 1;

    private float currentPulseAmount;

    private Coroutine coroutine_Pulse;

    public void SendPulse()
    {
        if (coroutine_Pulse != null)
            StopCoroutine(coroutine_Pulse);

        coroutine_Pulse = StartCoroutine(Pulse(pulseSpeed, pulseDuration, pulsePower));
    }

    private IEnumerator Pulse(float speed, float duration, float power)
    {
        float timer = 0;

        while (timer < speed)
        {
            timer += Time.deltaTime;

            currentPulseAmount = Mathf.Lerp(currentPulseAmount, power, timer / speed);

            onPulse?.Invoke(currentPulseAmount);

            yield return null;
        }

        timer = 0;

        while (timer < duration)
        {
            timer += Time.deltaTime;

            currentPulseAmount = Mathf.Lerp(power, 0, timer / duration);

            onPulse?.Invoke(currentPulseAmount);

            yield return null;
        }

        currentPulseAmount = 0;

        onPulse?.Invoke(currentPulseAmount);

        coroutine_Pulse = null;
    }
}