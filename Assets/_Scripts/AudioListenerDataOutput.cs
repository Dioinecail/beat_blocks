﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioListenerDataOutput : MonoBehaviour
{
    public float outputMultiplier = 25;

    [Range(0, 1024)]
    public int beatSampleFreq;
    [Range(0, 0.05f)]
    public float beatSampleThreshold;

    public UnityEvent onBeat;

    private float beatSample;
    private bool wasOnBeat;

    [Range(0, 1024)]
    public int snareSampleFreq;
    [Range(0, 0.05f)]
    public float snareSampleThreshold;

    public UnityEvent onSnare;

    private float snareSample;
    private bool wasOnSnare;

    #region Averages

    private float averageValue;
    private float averageDifference;

    private float tempAverageValue;
    private float tempAverageDifference;

    #endregion

    private float[] samples = new float[1024];

    private void Update()
    {
        AudioListener.GetSpectrumData(samples, 0, FFTWindow.Hamming);

        beatSample = samples[beatSampleFreq];

        if (beatSample > beatSampleThreshold)
        {
            if(!wasOnBeat)
            {
                wasOnBeat = true;
                onBeat?.Invoke();
            }
        }
        else
        {
            wasOnBeat = false;
        }

        snareSample = samples[snareSampleFreq];

        if (snareSample > snareSampleThreshold)
        {
            if (!wasOnSnare)
            {
                wasOnSnare = true;
                onSnare?.Invoke();
            }
        }
        else
        {
            wasOnSnare = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;

        tempAverageValue = samples[0];
        tempAverageDifference = 0;

        for (int i = 1; i < samples.Length; i++)
        {
            tempAverageValue += samples[i];
            tempAverageDifference += (i % 2 == 0) ? (samples[i] - samples[i - 1]) : 0;
            Gizmos.DrawLine(new Vector3(0, samples[i] * outputMultiplier, i), new Vector3(0, samples[i - 1] * outputMultiplier, i - 1));
        }

        averageValue = Mathf.Abs(tempAverageValue / samples.Length);
        averageDifference = Mathf.Abs(tempAverageDifference / (samples.Length / 2));
        beatSample = samples[beatSampleFreq];

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(-1, 0, 0), new Vector3(-1, averageValue * outputMultiplier, 0));

        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(-2, 0, 0), new Vector3(-2, averageDifference * outputMultiplier, 0));

        if(beatSample > beatSampleThreshold)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(new Vector3(-3, 0, 0), new Vector3(-3, beatSample * outputMultiplier, 0));
        }

        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(0, 0, beatSampleFreq), new Vector3(0, outputMultiplier, beatSampleFreq));

        Gizmos.DrawLine(new Vector3(0, beatSampleThreshold * outputMultiplier, beatSampleFreq + 1), new Vector3(0, beatSampleThreshold * outputMultiplier, beatSampleFreq - 1));

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(0, 0, snareSampleFreq), new Vector3(0, outputMultiplier, snareSampleFreq));

        Gizmos.DrawLine(new Vector3(0, snareSampleThreshold * outputMultiplier, snareSampleFreq + 1), new Vector3(0, snareSampleThreshold * outputMultiplier, snareSampleFreq - 1));
    }
}
