﻿using UnityEngine;
using System.Collections;

public class ClothOnWind : MonoBehaviour
{
    public Rigidbody rBody;
    public float velocityMultiplier = 1;

    private Cloth cloth;

    private void Start()
    {
        cloth = GetComponent<Cloth>();
    }

    void Update()
    {
        AccelerateClothBasedOnVelocity();
    }

    private void AccelerateClothBasedOnVelocity()
    {
        cloth.externalAcceleration = -rBody.velocity * velocityMultiplier;
    }
}