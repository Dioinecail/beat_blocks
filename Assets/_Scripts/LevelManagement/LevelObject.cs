﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Actual in game level gameobject
/// that has a few events when a level is loaded, unloaded, entered, exited etc.
/// </summary>
public class LevelObject : MonoBehaviour
{
    public static event Action<LevelObject> onLevelLoaded;
    public static event Action<LevelObject> onLevelUnloaded;
    public static event Action<LevelObject> onLevelEnter;
    public static event Action<LevelObject> onLevelExit;

    public List<int> connectedLevels;
    public int levelIndex;
    public Transform levelRoot;
    public Vector2 position;
    public Vector2[] spawnPoints;



    public Vector2 GetCameraPosition()
    {
        Vector2 pos = position + (Vector2)GetComponent<BoxCollider>().center;

        return pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            OnLevelEnter();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            OnLevelExit();
        }
    }

    private void OnLevelEnter()
    {
        onLevelEnter?.Invoke(this);

        levelRoot.gameObject.SetActive(true);
        LevelManager.LoadLevels(connectedLevels);
    }

    private void OnLevelExit()
    {
        onLevelExit?.Invoke(this);
        levelRoot.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        if (!LevelManager.AnythingLoaded())
        {
            GameManager.activeSpawnPoint = transform.TransformPoint(spawnPoints[0]);
            LevelManager.LoadMainScene(levelIndex);
        }

        onLevelLoaded?.Invoke(this);

        transform.position = position;
        GetComponent<Collider>().enabled = true;
    }

    private void OnDestroy()
    {
        onLevelUnloaded?.Invoke(this);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Gizmos.DrawCube(transform.TransformPoint(spawnPoints[i]), Vector3.one);
        }
    }

    private void OnValidate()
    {
        transform.position = position;
    }
}