﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(LevelObject))]
public class LevelObjectEditor : Editor
{
    private LevelObject level;



    private void OnEnable()
    {
        level = target as LevelObject;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.BeginHorizontal();

        DrawLoadButton();
        DrawSaveButton();

        EditorGUILayout.EndHorizontal();
    }

    private void DrawLoadButton()
    {
        if(GUILayout.Button("Open connected levels"))
        {
            foreach (int level in level.connectedLevels)
            {
                EditorSceneManager.OpenScene(LevelManager.GetFullScenePath(level), OpenSceneMode.Additive);
            }
        }
    }

    private void DrawSaveButton()
    {
        if (GUILayout.Button("Save and close connected levels"))
        {
            foreach (int level in level.connectedLevels)
            {
                EditorSceneManager.SaveOpenScenes();
                Scene scene = EditorSceneManager.GetSceneByPath(LevelManager.GetFullScenePath(level));
                EditorSceneManager.CloseScene(scene, true);
            }
        }
    }
}