﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LevelManager
{
    private const string levelName = "Level";
    private const string fullLevelName = "Assets/_Scenes/_Levels/Level";
    private const string mainSceneName = "Level_Main";
    public static event Action onBeforeUnloadingLevels;

    private static int activeLevel = -1;
    private static List<int> loadedLevels = new List<int>();



    public static void Init()
    {
        LevelObject.onLevelEnter += OnLevelEnter;
        LevelObject.onLevelExit += OnLevelExit;
    }

    public static void LoadLevels(List<int> levels)
    {
        onBeforeUnloadingLevels?.Invoke();

        for (int i = 0; i < loadedLevels.Count; i++)
        {
            if (levels.Contains(loadedLevels[i]) || loadedLevels[i] == activeLevel)
                continue;

            UnloadLevel(loadedLevels[i]);
        }

        for (int i = 0; i < levels.Count; i++)
        {
            if (loadedLevels.Contains(levels[i]))
                continue;

            LoadLevel(levels[i]);
        }
    }

    public static void LoadStartingLevel(int asset)
    {
        activeLevel = asset;

        LoadLevel(asset);
    }

    public static void LoadLevel(int asset)
    {
        loadedLevels.Add(asset);

        string scenePath = GetScenePath(asset);

        SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Additive);
    }

    public static void UnloadLevel(int asset)
    {
        loadedLevels.Remove(asset);

        string scenePath = GetScenePath(asset);

        SceneManager.UnloadSceneAsync(scenePath);
    }

    public static void LoadMainScene(int loadingFromIndex)
    {
        SetActiveLevel(loadingFromIndex);
        SceneManager.LoadSceneAsync(mainSceneName, LoadSceneMode.Additive);
    }

    public static bool AnythingLoaded()
    {
        return activeLevel > -1;
    }

    public static string GetScenePath(int levelIndex)
    {
        return levelName + levelIndex;
    }

    public static string GetFullScenePath(int levelIndex)
    {
        return fullLevelName + levelIndex + ".unity";
    }

    private static void OnLevelEnter(LevelObject target)
    {
        SetActiveLevel(target.levelIndex);


    }

    private static void SetActiveLevel(int index)
    {
        activeLevel = index;
        loadedLevels.Add(index);
    }

    private static void OnLevelExit(LevelObject target)
    {

    }
}