﻿using UnityEngine;

public class CollisionBehaviour : MonoBehaviour
{
    // referenses

    // paremeters
    public Vector2[] StickToWallOffset;
    public float handRadius = 0.25f;

    public Vector2 feetOffset = new Vector2(0, 0.06f);
    public float FeetRadius = 0.18f;

    public Vector2 angleCheckOffset = new Vector2(0, 0.185f);
    public float maxSlopeAngle = 55;

    public bool onGround;
    public bool onLeftWall;
    public bool onRightWall;
    public bool onWall
    {
        get
        {
            return onLeftWall || onRightWall;
        }
    }

    public LayerMask GroundMask;
    public Quaternion angleUnderFeet;

    // cache
    //protected Animator Anima;
    private Collider[] groundBuffer = new Collider[4];
    private RaycastHit[][] wallBuffer = new RaycastHit[4][];



    //protected virtual void Awake()
    //{
    //    Anima = GetComponentInChildren<Animator>();
    //}

    private void Update()
    {
        ClearBuffers();

        onLeftWall = false;
        onRightWall = false;

        Physics.OverlapSphereNonAlloc((Vector2)transform.position + feetOffset, FeetRadius, groundBuffer, GroundMask);

        for (int i = 0; i < StickToWallOffset.Length; i++)
        {
            onRightWall |= Physics.Raycast((Vector2)transform.position + StickToWallOffset[i], Vector3.right, handRadius, GroundMask);
            onLeftWall |= Physics.Raycast((Vector2)transform.position + StickToWallOffset[i], Vector3.left, handRadius, GroundMask);
        }

        onGround = groundBuffer[0] != null;

        //Anima.SetBool("OnGround", onGround);
        //Anima.SetBool("OnWall", (onLeftWall || onRightWall) && !onGround);
        RaycastBelow();
    }

    private void ClearBuffers()
    {
        for (int i = 0; i < groundBuffer.Length; i++)
        {
            groundBuffer[i] = null;
        }

        for (int i = 0; i < wallBuffer.GetLength(0); i++)
        {
            wallBuffer[i] = new RaycastHit[1];
        }
    }

    private void RaycastBelow()
    {
        RaycastHit hit;
        angleUnderFeet = Quaternion.identity;

        if (Physics.Raycast((Vector2)transform.position + angleCheckOffset, Vector3.down, out hit, 1, GroundMask) && onGround)
        {
            if(hit.collider != null)
            {
                float angle = Vector3.Angle(Vector3.up, hit.normal);
                if(angle < maxSlopeAngle)
                    angleUnderFeet = Quaternion.FromToRotation(Vector3.up, hit.normal);
                else
                    angleUnderFeet = Quaternion.identity;
            }
            else
            {
                angleUnderFeet = Quaternion.identity;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + feetOffset, FeetRadius);
        Gizmos.color = Color.yellow;

        foreach (var item in StickToWallOffset)
        {
            Gizmos.DrawWireSphere((Vector2)transform.position + item, handRadius);
        }
    }
}
