﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControlBehaviour : AbstractBehaviour
{
    public delegate void OnTimeChanged(float currentTime);

    public static event OnTimeChanged onTimeChanged;

    public bool calibrate;

    public float maxVelocity;

    private float fixedTimeStep;
    private float currentVelocity;

    protected override void Awake()
    {
        base.Awake();
        fixedTimeStep = Time.fixedDeltaTime;
    }

    private void Update()
    {
        if (calibrate)
            CalibrateMaxVelocity();
        else
            ReadVelocity();
    }

    private void ReadVelocity()
    {
        currentVelocity = rBody.velocity.magnitude;
        onTimeChanged?.Invoke(currentVelocity / maxVelocity);
    }

    private void CalibrateMaxVelocity()
    {
        if (rBody.velocity.magnitude > maxVelocity)
            maxVelocity = rBody.velocity.magnitude;
    }
}