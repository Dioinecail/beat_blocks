﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJumpBehaviour : AbstractBehaviour
{
    // parameters
    public float JumpSpeed = 5.5f;



    private void Update()
    {
        if (Input.GetButtonDown("Jump") && collisionBehaviour.onWall)
        {
            OnJump();
        }

        //Anima.SetFloat("VelocityY", rBody.velocity.y);
    }

    public void OnJump()
    {
        //Anima.Play("Jump");
        rBody.isKinematic = false;
        rBody.velocity = Quaternion.Euler(0, 0, collisionBehaviour.onRightWall ? 45 : -45) * new Vector3(rBody.velocity.x, JumpSpeed);
    }
}