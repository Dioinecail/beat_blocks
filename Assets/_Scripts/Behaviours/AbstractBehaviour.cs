﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBehaviour : MonoBehaviour
{
    // referenses
    public AbstractBehaviour[] behaviours;

    // cache
    protected Rigidbody rBody;
    protected CollisionBehaviour collisionBehaviour;

    protected virtual void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        collisionBehaviour = GetComponent<CollisionBehaviour>();
    }

    public void DisableBehaviours()
    {
        foreach (AbstractBehaviour behaviour in behaviours)
        {
            behaviour.enabled = false;  
        }
    }

    public void EnableBehaviours()
    {
        foreach (AbstractBehaviour behaviour in behaviours)
        {
            behaviour.enabled = true;
        }
    }
}
