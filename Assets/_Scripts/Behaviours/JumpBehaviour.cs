﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour
{
    public static event Action onJump;

    public float JumpSpeed = 5.5f;
    public float jumpMultiplier = 1;
    public float cachedVelocityMultiplier = 10;



    private void Update()
    {
        if (Input.GetButtonDown("Jump") && collisionBehaviour.onGround)
        {
            OnJump();
        }
    }

    public void OnJump()
    {
        Vector3 cachedVelocity = AttachBehaviour.GetCachedVelocity();

        if (cachedVelocity.y < 0)
            cachedVelocity = Vector3.zero;
        else
            cachedVelocity *= cachedVelocityMultiplier;

        rBody.isKinematic = false;
        rBody.velocity = new Vector3(rBody.velocity.x + cachedVelocity.x, JumpSpeed * jumpMultiplier + cachedVelocity.y);
        onJump?.Invoke();
    }
}
