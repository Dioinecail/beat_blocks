﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootBehaviour : AbstractBehaviour
{
    // events
    public UnityEvent onShotEvent;

    // referenses
    public GameObject projectilePrefab;
    public Transform gunTransform;

    // parameters
    public float aimSpeed = 5f;
    public float shootCooldown = 0.1f;
    public float shootOffset = 1;
    public float ammoSpeed = 1;
    public float damage = 20;
    public Vector3 aimingStartPosition;

    // cache
    private bool canShoot;
    private Coroutine coroutine_shooting;
    private Vector3 aimPosition;
    private Vector3 aimDirection;
    private Quaternion targetRotation;

    protected override void Awake()
    {
        base.Awake();

        canShoot = true;
    }

    private void Update()
    {
        Aim();

        if (Input.GetButton("Fire1") && canShoot)
        {
            OnShoot();
        }
    }

    public void OnShoot()
    {
        // Spawn an ammo
        GameObject po = GameobjectPoolSystem.Instantiate(projectilePrefab, aimPosition, targetRotation);
        Projectile projectile = po.GetComponent<Projectile>();
        projectile.SetDirection(aimDirection * ammoSpeed);
        projectile.SetDamage(damage);

        onShotEvent?.Invoke();

        if (coroutine_shooting != null)
            StopCoroutine(coroutine_shooting);

        coroutine_shooting = StartCoroutine(ShootCooldown());
    }

    private void Aim()
    {
        // find the target position that the player is looking at
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 mousePosition = mouseRay.origin + mouseRay.direction * -Camera.main.transform.position.z;

        aimDirection = mousePosition - (transform.position + aimingStartPosition);
        aimDirection.z = 0;
        aimDirection.Normalize();

        aimPosition = transform.position + (aimingStartPosition + aimDirection * shootOffset);

        targetRotation = Quaternion.LookRotation(aimDirection, Vector3.up);
        gunTransform.position = aimPosition;
        gunTransform.rotation = targetRotation;
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        DisableBehaviours();
        yield return new WaitForSeconds(shootCooldown);
        EnableBehaviours();
        canShoot = true;
    }

    private void OnDrawGizmos()
    {
        if (aimDirection.magnitude > 0.1f && aimPosition.magnitude > 0.1f)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position + aimingStartPosition, transform.position + aimingStartPosition + aimDirection * shootOffset);
        }
    }
}