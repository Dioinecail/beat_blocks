﻿using UnityEngine;

public class MoveBehaviour : AbstractBehaviour
{
    // referenses
    public Transform model;

    // parameters
    public float moveSpeed = 5;

    // cache
    private new Transform transform;
    private float moveDirection;
    private float yAngle = 180;



    protected override void Awake()
    {
        base.Awake();

        transform = GetComponent<Transform>();
    }

    private void Update()
    {
        GetInput();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void GetInput()
    {
        moveDirection = Input.GetAxis("Horizontal");
    }

    private void Move()
    {
        rBody.velocity = new Vector3(moveDirection * moveSpeed, rBody.velocity.y);

        if (Mathf.Abs(moveDirection) > 0.01f)
        {
            yAngle = rBody.velocity.x > 0 ? 90 : -90;
        }

        Quaternion targetRotation = Quaternion.Euler(0, yAngle, 0);
        model.rotation = Quaternion.Lerp(model.rotation, transform.rotation * targetRotation, 1);
    }
}
