﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachBehaviour : AbstractBehaviour
{
    public LayerMask attachMask;
    public float velocityBoost;
    public float jumpBoostTime;
    public float cachedVelocitySmoothing;

    private Transform attachedTransform;
    private Vector3 deltaVelocity;
    private Vector3 lastPosition;
    private Coroutine coroutine_Jump;
    private static Vector3 cachedVelocity;
    private Transform attachTransform;



    public static Vector3 GetCachedVelocity()
    {
        return cachedVelocity;
    }

    private void Update()
    {
        if(attachedTransform != null)
            attachTransform.position = attachedTransform.position;
    }

    private void LateUpdate()
    {
        if(attachedTransform != null)
        {
            attachTransform.position = attachedTransform.position;
            deltaVelocity = attachedTransform.position - lastPosition;
            lastPosition = attachedTransform.position;

            if(cachedVelocity.magnitude < deltaVelocity.magnitude)
            {
                cachedVelocity = deltaVelocity;
            }
            else
            {
                cachedVelocity = Vector3.Lerp(cachedVelocity, deltaVelocity, cachedVelocitySmoothing);
            }
        }
        else
        {
            cachedVelocity = Vector3.Lerp(cachedVelocity, Vector3.zero, cachedVelocitySmoothing);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        int checkLayer = attachMask.value;

        if(checkLayer == (checkLayer | 1 << other.gameObject.layer))
        {
            attachedTransform = other.transform;
            attachTransform.position = attachedTransform.position;
            lastPosition = attachedTransform.position;
            transform.SetParent(attachTransform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (coroutine_Jump != null)
            return;

        int checkLayer = attachMask.value;

        if (checkLayer == (checkLayer | 1 << other.gameObject.layer))
        {
            transform.SetParent(null);
            attachedTransform = null;
        }
    }

    private void OnJump()
    {
        if(attachedTransform != null)
        {
            if (coroutine_Jump != null)
                StopCoroutine(coroutine_Jump);

            coroutine_Jump = StartCoroutine(OnJumpCoroutine());
        }
    }

    private void OnEnable()
    {
        JumpBehaviour.onJump += OnJump;
        attachTransform = new GameObject("AttachingTransformProxy").transform;
    }

    private void OnDisable()
    {
        JumpBehaviour.onJump -= OnJump;
    }

    private IEnumerator OnJumpCoroutine()
    {
        transform.SetParent(null);
        float maxVelocity = cachedVelocity.magnitude;
        Vector3 velocity = cachedVelocity;

        float timer = 0;

        while(timer < jumpBoostTime)
        {
            timer += Time.deltaTime;

            if(cachedVelocity.magnitude > maxVelocity)
            {
                maxVelocity = cachedVelocity.magnitude;
                velocity = cachedVelocity;
            }

            yield return null;
        }
        //if(velocity.y > 0)
        //    rBody.AddForce(velocity * velocityBoost, ForceMode.Impulse);

        transform.SetParent(null);
        attachedTransform = null;
        coroutine_Jump = null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, deltaVelocity * 100);
        Gizmos.DrawRay(transform.position + Vector3.right * 0.25f, cachedVelocity * 100);
    }
}
