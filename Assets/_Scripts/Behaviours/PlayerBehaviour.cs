﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : AbstractBehaviour
{
    public GameObject[] Limbs;
    public AnimationEventSender animationEvents;



    public void OnDie(float explodeForce = 0, bool lost = true)
    {
        rBody.isKinematic = true;
        Destroy(GetComponent<CapsuleCollider>());
        foreach (GameObject limb in Limbs)
        {
            limb.GetComponent<Collider>().enabled = true;

            Rigidbody limbBody = limb.AddComponent<Rigidbody>();
            if (limbBody != null)
            {
                limbBody.mass = 5f;
                limb.transform.SetParent(null);
                limbBody.AddExplosionForce(explodeForce, transform.position, 15);
            }
        }
        animationEvents.LoseEvent();

        Destroy(this);
    }
}
