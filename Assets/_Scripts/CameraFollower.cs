﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    // referenses
    public Transform target;

    // parameters
    public float CameraSpeed = 5;
    public float mouseLookDistance = 1;
    public Vector2 Offset;

    // cache
    private Camera mainCam;
    private Vector2 currentOffset;
    private Ray mouseRay;



    private void Awake()
    {
        mainCam = GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        //if(target != null)
        //{
        //    FollowTarget();
        //    ExtendCameraTowardsCursor();
        //}
    }

    private void FollowTarget()
    {
        Vector3 targetPosition = target.position;
        targetPosition.z = transform.position.z;
        targetPosition += (Vector3)(Offset + currentOffset * mouseLookDistance);
        transform.position = Vector3.Lerp(transform.position, targetPosition, CameraSpeed * Time.fixedDeltaTime);
    }

    private void ExtendCameraTowardsCursor()
    {
        mouseRay = mainCam.ScreenPointToRay(Input.mousePosition);
        currentOffset = new Vector2(mouseRay.direction.x, mouseRay.direction.y);
    }

    private void SetCameraPosition(LevelObject level)
    {
        Vector3 position = level.GetCameraPosition();
        position.z = transform.position.z;

        transform.position = position;
    }

    private void OnEnable()
    {
        LevelObject.onLevelEnter += SetCameraPosition;
    }

    private void OnDisable()
    {
        LevelObject.onLevelEnter -= SetCameraPosition;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(mouseRay.origin, mouseRay.origin + mouseRay.direction);
    }
}