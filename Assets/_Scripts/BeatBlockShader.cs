﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BeatBlockShader : MonoBehaviour
{
    // reference
    public Material beatBlockMaterial;
    public Transform player;

    // parameters
    public float pulsePower = 0.2f;
    public float glowMax;
    [Range(0, 1)]
    public float currentGlow;

    void Update()
    {
        if(beatBlockMaterial != null && player != null)
        {
            SetPlayerPosition();
            SetShaderGlow();
        }
    }

    public void SetPlayerPosition()
    {
        beatBlockMaterial.SetVector("_PlayerPosition", player.position);
    }

    public void SetShaderGlow()
    {
        beatBlockMaterial.SetFloat("_MinimumDistance", currentGlow * glowMax);
    }

    //public void PulsePullAmount(float amount)
    //{
    //    beatBlockMaterial.SetFloat("_PullPower", amount * pulsePower);
    //}

    //private void OnEnable()
    //{
    //    PulseEventSender.onPulse += PulsePullAmount;
    //}

    //private void OnDisable()
    //{
    //    PulseEventSender.onPulse -= PulsePullAmount;
    //}
}