﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothGizmo : MonoBehaviour
{
    public Action onValidate;

    public int ClothPinIndex { get; set; } = -1;



    public void CreateChild(Vector3 position)
    {
        ClothGizmoChild child = new GameObject("gizmo").AddComponent<ClothGizmoChild>();
        child.onValidate += OnValidate;
        child.transform.SetParent(transform);
        child.transform.localPosition = position;
    }

    public void AddEvent(Action evt)
    {
        evt += OnValidate;
    }

    private void Update()
    {
        if (transform.hasChanged)
            OnValidate();
    }

    private void OnValidate()
    {
        onValidate?.Invoke();
    }
}