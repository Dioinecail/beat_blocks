using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeBridge
{
    public struct RopeSegment
    {
        public Vector3 posNow;
        public Vector3 posOld;

        public RopeSegment(Vector3 pos)
        {
            posNow = pos;
            posOld = pos;
        }
    }

    public Transform StartPoint { get; set; }
    public Transform EndPoint { get; set; }

    public bool IsActive { get; private set; }
    private List<RopeSegment> ropeSegments = new List<RopeSegment>();
    private float ropeSegLen = 0.25f;
    private int segmentsCount = 35;
    private AnimationCurve weightCurve;
    private Transform[] ropeBones;
    private Vector3 forceGravity;
    private float movementReduction = 1;



    public void InitRope(Transform startPoint, Transform endPoint, int segmentsCount, float segmentLength, float ropeGravity = -1, float movementReduction = 1)
    {
        StartPoint = startPoint;
        EndPoint = endPoint;

        Vector3 ropeStartPoint = StartPoint.position;
        this.segmentsCount = segmentsCount;
        this.ropeSegLen = segmentLength;
        this.movementReduction = movementReduction;

        forceGravity = new Vector3(0, ropeGravity);

        for (int i = 0; i < segmentsCount; i++)
        {
            ropeSegments.Add(new RopeSegment(ropeStartPoint));
        }

        IsActive = true;
    }

    public void InitRope(Transform startPoint, int segmentsCount, float segmentLength, float ropeGravity = -1, float movementReduction = 1)
    {
        StartPoint = startPoint;

        Vector3 ropeStartPoint = StartPoint.position;
        this.segmentsCount = segmentsCount;
        this.ropeSegLen = segmentLength;
        this.movementReduction = movementReduction;

        for (int i = 0; i < segmentsCount; i++)
        {
            ropeSegments.Add(new RopeSegment(ropeStartPoint));
        }

        forceGravity = new Vector3(0, ropeGravity);

        IsActive = true;
    }

    public void SetBones(Transform[] bones, AnimationCurve curve)
    {
        ropeBones = bones;
        weightCurve = curve;    
    }

    public void Reset()
    {
        IsActive = false;
        ropeSegments.Clear();
    }

    public void Simulate()
    {
        for (int i = 1; i < segmentsCount; i++)
        {
            RopeSegment firstSegment = ropeSegments[i];
            Vector3 velocity = (firstSegment.posNow - firstSegment.posOld) * movementReduction;
            firstSegment.posOld = firstSegment.posNow;
            firstSegment.posNow += velocity;
            firstSegment.posNow += forceGravity * Time.fixedDeltaTime;
            ropeSegments[i] = firstSegment;
        }

        //CONSTRAINTS
        for (int i = 0; i < 50; i++)
        {
            ApplyConstraint();
        }
    }

    public Vector3[] GetRopePositions()
    {
        Vector3[] ropePositions = new Vector3[segmentsCount];
        for (int i = 0; i < segmentsCount; i++)
        {
            ropePositions[i] = ropeSegments[i].posNow;
        }

        return ropePositions;
    }

    private void ApplyConstraint()
    {
        //Constrant to First Point 
        RopeSegment firstSegment = ropeSegments[0];
        firstSegment.posNow = StartPoint.position;
        ropeSegments[0] = firstSegment;

        //Constrant to Second Point 
        if(EndPoint != null)
        {
            RopeSegment endSegment = ropeSegments[ropeSegments.Count - 1];
            endSegment.posNow = EndPoint.position;
            ropeSegments[ropeSegments.Count - 1] = endSegment;
        }

        for (int i = 0; i < segmentsCount - 1; i++)
        {
            RopeSegment firstSeg = ropeSegments[i];
            RopeSegment secondSeg = ropeSegments[i + 1];

            float dist = (firstSeg.posNow - secondSeg.posNow).magnitude;
            float error = Mathf.Abs(dist - ropeSegLen);

            Vector3 changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;

            if (dist < ropeSegLen)
            {
                changeDir = -changeDir;
            }

            Vector3 changeAmount = changeDir * error;

            if (i != 0)
            {
                if(ropeBones != null && i < ropeBones.Length)
                {
                    float boneWeight = weightCurve.Evaluate((float)i / ropeBones.Length);
                    Vector3 position = Vector3.Lerp(firstSeg.posNow - changeAmount * 0.5f, ropeBones[i].position, boneWeight);

                    firstSeg.posNow = position;
                    ropeSegments[i] = firstSeg;

                    secondSeg.posNow += changeAmount * 0.5f;
                    ropeSegments[i + 1] = secondSeg;
                }
                else
                {
                    firstSeg.posNow -= changeAmount * 0.5f;
                    ropeSegments[i] = firstSeg;

                    secondSeg.posNow += changeAmount * 0.5f;
                    ropeSegments[i + 1] = secondSeg;
                }
            }
            else
            {
                secondSeg.posNow += changeAmount;
                ropeSegments[i + 1] = secondSeg;
            }
        }
    }
}