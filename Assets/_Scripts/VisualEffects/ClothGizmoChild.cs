﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothGizmoChild : MonoBehaviour
{
    public event Action onValidate;



    private void Update()
    {
        if (transform.hasChanged)
            OnValidate();
    }

    private void OnEnable()
    {
        ClothGizmo parentGizmo = GetComponentInParent<ClothGizmo>();
        if (parentGizmo != null)
            parentGizmo.AddEvent(onValidate);
    }

    private void OnValidate()
    {
        onValidate?.Invoke();
    }
}