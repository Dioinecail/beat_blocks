﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(VerletClothRenderer))]
public class VerletClothEditor : Editor
{
    private VerletClothRenderer targetCloth;
    private Transform[] gizmosTransforms;



    private void OnEnable()
    {
        targetCloth = (VerletClothRenderer)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();

        if(GUILayout.Button("Create Gizmos Transforms"))
        {
            CreateGizmosForEditing();
        }

        if (GUILayout.Button("Delete Gizmos Transforms"))
        {
            ClearEditingGizmos();
        }

        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Apply Gizmos Values"))
        {
            ApplyGizmosValues();
        }

        EditorGUILayout.EndVertical();
    }

    private void CreateGizmosForEditing()
    {
        for (int i = 0; i < targetCloth.clothPins.Length; i++)
        {
            Transform gizmoParent = new GameObject("Gizmo_Parent").transform;
            ClothGizmo newGizmo = gizmoParent.gameObject.AddComponent<ClothGizmo>();
            int index = i;
            newGizmo.ClothPinIndex = index;
            newGizmo.onValidate += ApplyGizmosValues;

            gizmoParent.SetPositionAndRotation(targetCloth.clothPins[i].origin.position, targetCloth.clothPins[i].origin.rotation);

            for (int x = 0; x < targetCloth.clothPins[i].Count; x++)
            {
                newGizmo.CreateChild(targetCloth.clothPins[i].offsets[x]);
            }
        }
    }

    public void ApplyGizmosValues()
    {
        ClothGizmo[] gizmos = FindObjectsOfType<ClothGizmo>();

        for (int i = 0; i < targetCloth.clothPins.Length; i++)
        {
            ClothGizmo targetGizmo = gizmos.Where(x => x.ClothPinIndex == i).First();
            int newGizmosCount = targetGizmo.transform.childCount;
            targetCloth.clothPins[i].offsets = new Vector3[newGizmosCount];

            for (int j = 0; j < newGizmosCount; j++)
            {
                targetCloth.clothPins[i].offsets[j] = targetGizmo.transform.GetChild(j).localPosition;
            }
        }
    }

    private void ClearEditingGizmos()
    {
        ClothGizmo[] gizmos = FindObjectsOfType<ClothGizmo>();

            for (int i = 0; i < gizmos.Length; i++)
            {
                if(Application.isPlaying)
                    Destroy(gizmos[i].gameObject);
                else
                    DestroyImmediate(gizmos[i].gameObject);
            }
    }
}