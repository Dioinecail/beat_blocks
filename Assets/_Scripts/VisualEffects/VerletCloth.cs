﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClothBone
{
    public Transform origin;
    public Vector3[] offsets;

    public AnimationCurve weightCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));

    public Vector3 this[int index]
    {
        get { return origin.TransformPoint(offsets[index]); }
    }
    public int Count {  get { return offsets.Length; } }
}

public class VerletCloth
{
    public struct ClothSegment
    {
        public Vector3 posNow;
        public Vector3 posOld;

        public ClothSegment(Vector3 pos)
        {
            posNow = pos;
            posOld = pos;
        }
    }

    public bool IsActive { get; private set; }

    private float outwardForceCompensation = 1;
    private int constraintRate = 50;
    private float segmentLength = 0.25f;
    private int segmentCountX = 35;
    private int segmentCountY = 35;
    private Vector3 gravity;
    private float movementReduction = 1;
    private ClothSegment[,] cloth;
    private ClothBone[] pins;
    private AnimationCurve weightCurve;



    public VerletCloth(VerletClothSettings settings)
    {
        this.pins = settings.pins;

        segmentCountX = settings.pinsCount;
        segmentCountY = settings.segmentCountY;

        cloth = new ClothSegment[segmentCountX, segmentCountY];

        this.gravity = new Vector3(0, settings.gravity, 0);
        this.movementReduction = settings.movementReduction;
        this.segmentLength = settings.segmentLength;
        this.constraintRate = settings.constraintRate;
        this.outwardForceCompensation = settings.errorCompensation;
        this.weightCurve = settings.weightCurve;

        for (int x = 0; x < segmentCountX; x++)
        {
            for (int y = 0; y < segmentCountY; y++)
            {
                cloth[x, y] = new ClothSegment(pins[x][0]);
            }
        }

        IsActive = true;
    }

    public void Simulate()
    {
        for (int x = 0; x < segmentCountX; x++)
        {
            for (int y = 0; y < segmentCountY; y++)
            {
                ClothSegment segment = cloth[x, y];

                Vector3 velocity = (segment.posNow - segment.posOld) * movementReduction;

                segment.posOld = segment.posNow;
                segment.posNow += velocity;
                segment.posNow += gravity * Time.fixedDeltaTime;

                cloth[x, y] = segment;
            }
        }

        //CONSTRAINTS
        for (int i = 0; i < constraintRate; i++)
        {
            ApplyConstraints();
        }
    }

    public Vector3[] GetClothPoints(Transform local)
    {
        Vector3[] points = new Vector3[segmentCountX * segmentCountY];

        if(local != null)
        {
            for (int i = 0, y = 0; y < segmentCountY; y++)
            {
                for (int x = 0; x < segmentCountX; x++, i++)
                {
                    points[i] = local.InverseTransformPoint(cloth[x, y].posNow);
                }
            }
        }
        else
        {
            for (int i = 0, y = 0; y < segmentCountY; y++)
            {
                for (int x = 0; x < segmentCountX; x++, i++)
                {
                    points[i] = cloth[x, y].posNow;
                }
            }
        }

        return points;
    }

    private void ApplyConstraints()
    {
        ApplyPinnedConstraints();
        ApplyHorizontalConstraints();
        ApplyVerticalConstraints();
    }

    private void ApplyPinnedConstraints()
    {
        for (int x = 0; x < segmentCountX; x++)
        {
            Vector3 position = pins[x][0];

            ClothSegment segment = cloth[x, 0];
            segment.posNow = position;
            cloth[x, 0] = segment;
        }
    }

    private void ApplyHorizontalConstraints()
    {
        for (int x = 0; x < segmentCountX - 1; x++)
        {
            for (int y = 1; y < segmentCountY; y++)
            {
                ClothSegment currentSeg = cloth[x, y];
                ClothSegment nextSeg = cloth[x + 1, y];

                float dist = (currentSeg.posNow - nextSeg.posNow).magnitude;
                float error = Mathf.Abs(dist - segmentLength);

                Vector3 changeDir = (currentSeg.posNow - nextSeg.posNow).normalized;

                if (dist < segmentLength)
                {
                    changeDir = -changeDir;
                    error *= outwardForceCompensation;
                }

                Vector3 changeAmount = changeDir * error;

                if(x == 0 && y == 0)
                {
                    nextSeg.posNow += changeAmount * 0.5f;
                    cloth[x + 1, y] = nextSeg;
                }
                else
                {
                    currentSeg.posNow -= changeAmount * 0.25f;
                    cloth[x, y] = currentSeg;

                    nextSeg.posNow += changeAmount * 0.25f;
                    cloth[x + 1, y] = nextSeg;
                }
            }
        }
    }

    private void ApplyVerticalConstraints()
    {
        for (int x = 0; x < segmentCountX; x++)
        {
            for (int y = 0; y < segmentCountY - 1; y++)
            {
                float currentSegmentLength = segmentLength * weightCurve.Evaluate((float)x / segmentCountX);

                ClothSegment currentSeg = cloth[x, y];
                ClothSegment nextSeg = cloth[x, y + 1];

                float dist = (currentSeg.posNow - nextSeg.posNow).magnitude;
                float error = Mathf.Abs(dist - currentSegmentLength);

                Vector3 changeDir = (currentSeg.posNow - nextSeg.posNow).normalized;

                if (dist < currentSegmentLength)
                {
                    changeDir = -changeDir;
                    error *= outwardForceCompensation;
                }

                Vector3 changeAmount = changeDir * error;

                if (y == 0)
                {
                    nextSeg.posNow += changeAmount * 0.5f;
                    cloth[x, y + 1] = nextSeg;
                }
                else
                {
                    Vector3 position;
                    if (y < pins[x].Count)
                    {
                        float boneWeight = pins[x].weightCurve.Evaluate((float)y / pins[x].Count);
                        position = Vector3.Lerp(currentSeg.posNow - changeAmount * 0.25f, pins[x][y], boneWeight);
                    }
                    else
                    {
                        position = currentSeg.posNow - changeAmount * 0.25f;
                    }

                    currentSeg.posNow = position;
                    cloth[x, y] = currentSeg;

                    nextSeg.posNow += changeAmount * 0.25f;
                    cloth[x, y + 1] = nextSeg;
                }
            }
        }
    }
}
