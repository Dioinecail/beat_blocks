﻿using System.Collections.Generic;
using UnityEngine;

public class ParticleEmitter
{
    private static Dictionary<GameObject, ParticleSystem> particleDictionary = new Dictionary<GameObject, ParticleSystem>();



    public static void EmitParticle(GameObject prefab, Vector3 position)
    {
        EmitParticle(prefab, position, Quaternion.identity);
    }

    public static void EmitParticle(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        ParticleSystem particle;
        if (particleDictionary.ContainsKey(prefab))
        {
            particle = particleDictionary[prefab];
        }
        else
        {
            GameObject clone = Object.Instantiate(prefab, position, Quaternion.identity);
            particle = clone.GetComponent<ParticleSystem>();

            particleDictionary.Add(prefab, particle);
        }

        particle.gameObject.SetActive(true);
        particle.transform.position = position;
        particle.transform.rotation = rotation;
        particle.Play();
    }

    public static void Clear()
    {
        particleDictionary.Clear();
    }
}