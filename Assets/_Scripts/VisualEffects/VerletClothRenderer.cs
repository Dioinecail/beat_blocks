﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct VerletClothSettings
{
    public ClothBone[] pins;
    public int pinsCount;
    public int segmentCountY;
    public float segmentLength;
    public float gravity;
    public int constraintRate;
    public float movementReduction;
    public float errorCompensation;
    public AnimationCurve weightCurve;
}

public class VerletClothRenderer : MonoBehaviour
{
    public ClothBone[] clothPins;

    public int clothConstraintRate = 50;
    public float clothSegmentLength = 0.25f;
    public int clothSegmentCountX = 35;
    public int clothSegmentCountY = 35;
    public float clothGravity;
    [Range(0, 1f)] public float clothMovementReduction = 1;
    [Range(0, 1f)] public float clothOutwardForceCompensation = 1;
    public AnimationCurve clothWeightCurve;
    public bool visualize;
    public float visualizationRadius;

    private VerletCloth targetCloth;
    private Mesh clothMesh;
    private MeshFilter meshFilter;



    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
    }

    private void Start()
    {
        CreateClothMesh();

        VerletClothSettings settings = new VerletClothSettings();
        settings.pins = clothPins;
        settings.pinsCount = clothPins.Length;
        settings.segmentCountY = clothSegmentCountY;
        settings.segmentLength = clothSegmentLength;
        settings.gravity = clothGravity;
        settings.constraintRate = clothConstraintRate;
        settings.movementReduction = clothMovementReduction;
        settings.errorCompensation = clothOutwardForceCompensation;
        settings.weightCurve = clothWeightCurve;

        targetCloth = new VerletCloth(settings);
    }

    private void FixedUpdate()
    {
        targetCloth.Simulate();
    }

    private void Update()
    {
        ApplyClothVertices();
    }

    private void CreateClothMesh()
    {
        if (clothMesh != null)
            Destroy(clothMesh);

        clothMesh = MeshGenerator.CreateMesh(clothPins.Length - 1, clothSegmentCountY - 1);
        meshFilter.sharedMesh = clothMesh;
    }

    private void ApplyClothVertices()
    {
        Vector3[] points = targetCloth.GetClothPoints(transform);

        meshFilter.sharedMesh.SetVertices(points);
    }

    private void OnValidate()
    {
        if(Application.isPlaying)
        {
            meshFilter = GetComponent<MeshFilter>();
            Start();
        }
    }

    private void OnDrawGizmos()
    {
        if(visualize)
        {
            for (int i = 0; i < clothPins.Length; i++)
            {
                if(clothPins[i].origin != null)
                    for (int x = 0; x < clothPins[i].Count; x++)
                    {
                        Vector3 position = clothPins[i][x];

                        Gizmos.color = Color.Lerp(Color.red, Color.green, clothPins[i].weightCurve.Evaluate((float)x / clothPins[i].Count));
                        Gizmos.DrawWireSphere(position, visualizationRadius);
                    }
            }
        }
    }
}
