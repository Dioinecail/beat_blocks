using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairRenderer : MonoBehaviour
{
    public LineRenderer hairRenderer;
    public AnimationCurve hairWeightCurve;
    public Transform[] hairBones;

    public int hairSegments;
    public float hairSegmentLength;
    public float hairGravity;
    public float hairMovementReduction;

    private RopeBridge hairRope;



    private void Start()
    {
        hairRope = new RopeBridge();
        hairRope.InitRope(transform, hairSegments, hairSegmentLength, hairGravity, hairMovementReduction);
        hairRope.SetBones(hairBones, hairWeightCurve);
    }

    private void FixedUpdate()
    {
        hairRope.Simulate();
        Vector3[] hairPositions = hairRope.GetRopePositions();
        hairRenderer.positionCount = hairPositions.Length;
        hairRenderer.SetPositions(hairPositions);
    }

    private void OnValidate()
    {
        Start();
    }
}
